var ServHandler;
var PaqueteHandler;
var distancia=0,
	muestratarifa="NO",
	inilat='',
	inilon='',
	antlat='',
	antlon='',
	cont=0,
	horaini='',
	horaact='',
	minutos=0,
	carreraprecio=0,//valor inicial que viene desde el servidor cuando se logue el usuario
	tarifamin=0,//tarifa por minuto adicional
	tarifakm=0,//tarifa por kilometro
	minima=0,//tarifa por kilometro
	udimetrot=0,	
	fechaini=new Date(),
	calculado=0,
	nuevo=1,
	providerGps = Ti.Geolocation.Android.createLocationProvider({
    name: Ti.Geolocation.PROVIDER_GPS,
    minUpdateDistance: 50,
    minUpdateTime: 20
	}),
	gpsRule = Ti.Geolocation.Android.createLocationRule({
	    provider: Ti.Geolocation.PROVIDER_GPS,
	    minAge: 20000 // milliseconds
	});
	Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
	Ti.Geolocation.Android.addLocationProvider(providerGps);
	Ti.Geolocation.Android.addLocationRule(gpsRule);
	Ti.Geolocation.Android.manualMode = true;
exports.comenzar= function(e){
	ServHandler=require('ServicioHandler');
	PaqueteHandler=require('PaqueteHandler');
	muestratarifa=PaqueteHandler.getPaquete(ServHandler.getPaquete()).muestratarifa;
	//Ti.API.info("comienzo");
	Ti.App.Properties.setString('udimetro','on');
	nuevo=1;
	cont=0;
	minutos=0;
	calculado=0;
	carreraprecio=parseInt(ServHandler.getTarifa().comienzaen),//valor inicial que viene desde el servidor cuando se logue el usuario
	tarifamin=parseInt(ServHandler.getTarifa().tarifaundseg),//tarifa por minuto adicional
	minima=parseInt(ServHandler.getTarifa().minima),//tarifa por kilometro
	tarifakm=parseInt(ServHandler.getTarifa().tarifaundkm);
	carreraprecio=parseInt(ServHandler.getTarifa().comienzaen);
	//Ti.API.info("iniciando = " + JSON.stringify(carreraprecio));
	Titanium.Geolocation.addEventListener('location', locationCallback);
	
};
exports.retomar= function(e){
	ServHandler=require('ServicioHandler');
	PaqueteHandler=require('PaqueteHandler');
	nuevo=0;
	Ti.App.Properties.setString('udimetro','on');
	//Ti.API.info("retomando = " + JSON.stringify(ServHandler.getServicioUdimetro()));
	tarifamin=parseInt(ServHandler.getTarifa().tarifaundseg),//tarifa por minuto adicional
	minima=parseInt(ServHandler.getTarifa().minima),//tarifa por kilometro
	tarifakm=parseInt(ServHandler.getTarifa().tarifaundkm);
	antlat=ServHandler.getAntCoor().latitude;
	antlon=ServHandler.getAntCoor().longitude;
	muestratarifa=PaqueteHandler.getPaquete(ServHandler.getPaquete()).muestratarifa;
	distancia=ServHandler.getDistancia();
	if(ServHandler.getFechaini()!=""){
		fechaini= new Date(ServHandler.getFechaini());
	}else{
		fechaini=new Date();
	}
	
	carreraprecio=ServHandler.getPrecio();
	Titanium.Geolocation.addEventListener('location', locationCallback);
	
};
exports.detener = function(callback){
	//Ti.API.info("detener");
	Ti.App.Properties.setString('udimetro','off');
	if(calculado==0){
		Titanium.Geolocation.removeEventListener('location', locationCallback);
		Ti.Geolocation.getCurrentPosition(function(e) {
			var unit="K";
			if(!isNaN(parseFloat(e.coords.latitude)) && isFinite(e.coords.latitude) && !isNaN(parseFloat(e.coords.longitude)) && isFinite(e.coords.longitude)){
				var radlat1 = Math.PI * e.coords.latitude/180;
			    var radlat2 = Math.PI * antlat/180;
			    var radlon1 = Math.PI * e.coords.longitude/180;
			    var radlon2 = Math.PI * antlon/180;
			    var theta = e.coords.longitude-antlon;
			    var radtheta = Math.PI * theta/180;
			    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
			    dist = Math.acos(dist);
			    dist = dist * 180/Math.PI;
			    dist = dist * 60 * 1.1515;
			    if (unit=="K") { dist = dist * 1.609344; }
			    if (unit=="N") { dist = dist * 0.8684; }
			    			var f = new Date();
							var t = f.getTime(); 
							var h = (f.getHours()<10)? "0"+f.getHours():f.getHours();
							var m = (f.getMinutes()<10)? "0"+f.getMinutes():f.getMinutes();
							var s = (f.getSeconds()<10)? "0"+f.getSeconds():f.getSeconds();
		        horaact=h+':'+m+':'+s;
		        minutos=((t-(fechaini.getTime()))/60000);
			    if(dist!='NAN' && dist>=0){
			    	distancia=distancia+dist;
			    	carreraprecio=carreraprecio+(dist*tarifakm);
			    	if(muestratarifa=="SI"){	    						
			    	Ti.App.fireEvent("servicio:mostrarTarifa",{final:1});
			    	}
					//graba en memoria los datos actuales	
			    }
			    ServHandler.setFinServicio(e.coords,distancia,minutos,horaact);//guardamos distancia
			    calculado=1;
			    distancia=0,
				inilat='',
				inilon='',
				antlat='',
				antlon='',
				cont=0,
				horaini='',
				horaact='',
				minutos=0,
				carreraprecio=0,//valor inicial que viene desde el servidor cuando se logue el usuario
				tarifamin=0,//tarifa por minuto adicional
				tarifakm=0,//tarifa por kilometro
				minima=0,//tarifa por kilometro
				udimetrot=0,	
				fechaini=new Date(),
				calculado=0;
			    callback({resultado:"ok"});
			    
			}else{
				callback({resultado:"error"});
			}
		});
			
		
	}
		
};
function locationCallback(e){
        if (!e.success || e.error) {
            return;
        	} else {
             var unit="K";
		        if(cont==0 && nuevo==1){//si comienza nuevo servicio o retoma	
		        	antlat=e.coords.latitude;
					antlon=e.coords.longitude;
					inilat=e.coords.latitude;
					inilon=e.coords.longitude;
					cont++;			
					fechaini = new Date();
							var t = fechaini.getTime(); 
							var h = (fechaini.getHours()<10)? "0"+fechaini.getHours():fechaini.getHours();
							var m = (fechaini.getMinutes()<10)? "0"+fechaini.getMinutes():fechaini.getMinutes();
							var s = (fechaini.getSeconds()<10)? "0"+fechaini.getSeconds():fechaini.getSeconds();
							var year = fechaini.getFullYear();
							var month = (fechaini.getMonth()<10)? "0"+(fechaini.getMonth()+1):fechaini.getMonth()+1;
							var day = (fechaini.getDate()<10)? "0"+(fechaini.getDate()):fechaini.getDate();
							horaini=h+':'+m+':'+s;
							var fecha = year+'-'+month+'-'+day;
					ServHandler.setServicio(e.coords,horaini,fechaini,fecha);//configura coordenadas y hora iniciales
					ServHandler.setAntCoor(e.coords);//configura coordenadas anteriores
					
		        	} 
		        		
		        var radlat1 = Math.PI * e.coords.latitude/180;
			    var radlat2 = Math.PI * antlat/180;
			    var radlon1 = Math.PI * e.coords.longitude/180;
			    var radlon2 = Math.PI * antlon/180;
			    var theta = e.coords.longitude-antlon;
			    var radtheta = Math.PI * theta/180;
			    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
			    dist = Math.acos(dist);
			    dist = dist * 180/Math.PI;
			    dist = dist * 60 * 1.1515;
			    if (unit=="K") { dist = dist * 1.609344; }
			    if (unit=="N") { dist = dist * 0.8684; }
			    	var f = new Date();
					var t = f.getTime(); 
		       		minutos=((t-(fechaini.getTime()))/60000);
		       
		        //Ti.API.info('lat ' + e.coords.latitude +' long '+e.coords.longitude + ' distancia min' + dist +' distancia tot' + distancia +' y carreraprecio'+carreraprecio);	
			    if(dist!='NAN' && dist>=0){
			    	distancia=distancia+dist;
			    	carreraprecio=carreraprecio+(dist*tarifakm);
			    	ServHandler.setDistancia(distancia);//guardamos distancia
					ServHandler.setAntCoor(e.coords);//guardamos coordenadas anteriores
					ServHandler.setTiempo(minutos);//guardamos minutos
					if(muestratarifa=="SI"){
						Ti.App.fireEvent("servicio:mostrarTarifa",{final:0});
					}	
			        //Ti.API.info('lat ' + e.coords.latitude +' long '+e.coords.longitude + ' distancia min' + dist +' distancia tot' + distancia +' y carreraprecio'+carreraprecio);	       
					antlat=e.coords.latitude;
					antlon=e.coords.longitude;
					//graba en memoria los datos actuales
					
					
			    }
			    
        }
    };
