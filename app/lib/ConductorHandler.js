var Conductor={
	loggedIn:false,
};
if(Ti.App.Properties.hasProperty('Conductor')){
		Conductor=Ti.App.Properties.getList('Conductor');
	}
exports.setConductor = function(datos) {
	Conductor=datos;
	Ti.App.Properties.setList('Conductor',Conductor);
};
exports.getConductor = function(){
	return {
			id:Conductor.id,
			room:Conductor.room,
			placaact:Conductor.placaact,
			vehiculoact:Conductor.vehiculoact,
			paquetes:Conductor.paquetesact,
			servicioact:Conductor.servicioact,
			estado:Conductor.estadoapp,
			nombre:Conductor.nombre,
			documento:Conductor.documento,
			imei:Conductor.imei,
			img:Conductor.img,	
			};
};
exports.setLoggedIn = function(estado){
	Conductor.loggedIn=estado;
	Ti.App.Properties.setList('Conductor',Conductor);
};
/*
 * Cada vez que el conductor ingresa se verifica si esta logueado
 * En caso de forzar a cerrar la aplicacion queda la propiedad Conductor
 * y recupera el estado de esta.
 */ 
exports.getLoggedIn = function(){
	 //Ti.API.info("La contrasena es = "+Conductor.loggedIn);
	return Conductor.loggedIn;
};
exports.setPlaca = function(placa){
	Conductor.placaact=placa;
	for(var i=0;i<Conductor.vehiculos.length;++i){
		 ////Ti.API.info("La placa "+placa+"es igual a = "+Conductor.vehiculos[i]["placa"]);
		if(Conductor.vehiculos[i]["placa"]==placa){
			 ////Ti.API.info("Se ha configurado el vehiculos = "+JSON.stringify(Conductor.vehiculos[i]));
			Conductor.vehiculoact=Conductor.vehiculos[i];
			Ti.App.Properties.setList('Conductor',Conductor);
		}	
	}
	Ti.App.Properties.setList('Conductor',Conductor);
};
exports.getPlaca = function(){
	return Conductor.placaact;
};
exports.getVehiculo = function(){
	return Conductor.vehiculoact;
};
exports.getPlacas = function(){
	var placas=[];
	var vehiculo;
	 
	for(var i=0;i<Conductor.vehiculos.length;++i){
		vehiculo=Conductor.vehiculos[i]["placa"];
		placas.push(vehiculo);
		
	}
	////Ti.API.info("retorna placas = "+JSON.stringify(placas));
	return placas;
};
exports.setPaquetes = function(paquetes){
	Conductor.paquetesact=paquetes;
	Ti.App.Properties.setList('Conductor',Conductor);
	////Ti.API.info("La configuracion total del conductor es = "+JSON.stringify(Conductor));
};
exports.getPaquetes = function(){
	return Conductor.paquetesact;
};
exports.setEstado = function(estado){
	Conductor.estadoapp=estado;
	Ti.App.Properties.setList('Conductor',Conductor);
};
exports.getEstado = function(){
	return Conductor.estadoapp;
};
exports.setRoom = function(room){
	Conductor.room=room;
	Ti.App.Properties.setList('Conductor',Conductor);
};
exports.getRoom = function(){
	return Conductor.room;
};
exports.getTokenConductor=function(){
	fechaini = new Date();
    var year = fechaini.getFullYear();
    var month = fechaini.getMonth()+1;
    var day = fechaini.getDate();
    var fecha = year+'-'+month+'-'+day; 
	var pass=Ti.Utils.md5HexDigest(Conductor.password);
	var token=Ti.Utils.md5HexDigest(fecha+pass);
	return token;
};
exports.salir = function(){
Conductor={
	loggedIn:false,
};
Ti.App.Properties.setList('Conductor',Conductor);
};
exports.login = function(username, password, callback) {
       		var xhr=Titanium.Network.createHTTPClient();   
       		fechaini = new Date();
       		var year = fechaini.getFullYear();
			var month = fechaini.getMonth()+1;
			var day = fechaini.getDate();
			var fecha = year+'-'+month+'-'+day; 
			var pass=Ti.Utils.md5HexDigest(password);
			var token=Ti.Utils.md5HexDigest(fecha+pass);
			//Ti.API.info("la url es  = "+Alloy.Globals.server.api+'v1/conductoresplacas?token='+token+'&usuario='+username);
    		xhr.open('GET',Alloy.Globals.server.api+'v1/conductoresplacas?token='+token+'&usuario='+username); 	
    		xhr.onload = function(e){
    			var data = JSON.parse(this.responseText);
    			
                 //Ti.API.info("La respuesta de intelligent es = "+data.resultado);
                 
    			 if (data.resultado == 0) {
    			 	var conductor=data.datos.conductor[0];
    			 	var nombre2 = (conductor.nombre2!=undefined) ? conductor.nombre2 : '';
    			 	var apellido2 = (conductor.apellido2!=undefined) ? conductor.apellido2 : '';
					exports.setConductor({
									id:data.datos.conductor[0].id,
									loggedIn:true,
									room:"",
									vehiculos:data.datos.vehiculos,
									placaact:"",
									vehiculoact:"",
									paquetesact:"",
									servicioact:"",
									estado:conductor.estado,
									estadoapp:"LIBRE",
									nombre:conductor.nombre1+" "+nombre2+" "+conductor.apellido1+" "+apellido2,
									documento:conductor.documento,
									usuario:username,
									password:password,
									imei:Titanium.Platform.id,
									img:"",	
							});
					callback({ result: 'ok' });
                } else {
                	callback({ result: 'error', msg: "El usuario o la contraseña no es valido" });
                }
	     		
	                 
   		};
    	xhr.onerror = function(e){
    		callback({ result: 'error', msg: "No hay Red" });
    		};
    	xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    	xhr.setRequestHeader("Accept","application/json");
    	var array={
    					token:token,
    					usuario:username,
    				};
    	//xhr.send(JSON.stringify(array)); 
    	xhr.send();                    
};
