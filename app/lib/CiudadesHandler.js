var Ciudades={
	"Bogotá":"BOGOTA",
	"Cali":"CALI",
};
var Tarifa={
	tarifamin:0,
	tarifakm:0,
	minima:0,
	comienzaen:0,
};

exports.getServicioRoom = function(){
	return Servicio.room;
};
exports.setServicioRoom = function(room){
	Servicio.room=room;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.setFpago = function(fpago){
	Servicio.fpago=fpago;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.setPaquete = function(paquete){
	Servicio.paquete=paquete;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.setTarifa = function(tarifa){
	Tarifa=tarifa;
	Ti.App.Properties.setList('Tarifa',Tarifa);
};
exports.getTarifa = function(){
	return Tarifa;
};
exports.setServicio = function(coor,horaini){
	
	Servicio.inilat=coor.latitude;
	Servicio.inilon=coor.longitude;
	Servicio.horaini=time.horaini;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.setAntCoor = function(coor){
	Servicio.antlat=coor.latitude;
	Servicio.antlon=coor.longitude;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.setDistancia = function(distancia){
	Servicio.distancia=distancia;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.getDistancia = function(){
	return Servicio.distancia;
};
exports.setTiempo = function(minutos){
	Servicio.minutos=minutos;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.getTiempo = function(){
	return Servicio.minutos;
};
exports.getAntCoor = function(){
	return {latitude:Servicio.antlat,longitude:Servicio.antlon};
};
exports.setFinServicio = function(coor,distancia,tiempo){
	Servicio.finlat=coor.latitude;
	Servicio.finlon=coor.longitude;
	Servicio.distancia=distancia;
	Servicio.minutos=tiempo;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.setServicio = function(coor,distancia,tiempo){
	Servicio.finlat=coor.latitude;
	Servicio.finlon=coor.longitude;
	Servicio.distancia=distancia;
	Servicio.minutos=tiempo;
	Ti.App.Properties.setList('Servicio',Servicio);
};
/*
 * Cada vez que el conductor ingresa a la aplicacion recupera los datos anteriores
 */ 
exports.recoverConf = function(){
	Conf=Ti.App.Properties.getList('Conf');
	Servicio=Ti.App.Properties.getList('Servicio');
	Pasajero=Ti.App.Properties.getList('Pasajero');
	Tarifa=Ti.App.Properties.getList('Tarifa');
};

