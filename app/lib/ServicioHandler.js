var Sincronizado=false,
Sincronizar="",
SyncAbordar="",
SyncFuec="",
sincronizandofuec=false,
ServSync=[],
ServErrorSync=[],
fuecSync=[],
tiqueteSync=[],
logsCarrera=[],
Servicio={
	conductorid:"",
	vehiculo:"",
	pasajeroid:"",
	servicioid:"",
	tiqueteid:"",
	tarifasid:"",
	fuecsid:"",
	fuecurl:"",
	paquete:"",
	room:"",
	tiquete:"",
	fpago:"",
	fpagonom:"",
	token:"",
	muestratarifa:"",
	origen:"",
	destino:"",
	inilat:"",
	inilon:"",
	fecha:"",
	horaini:"",
	horafin:"",
	antlat:"",
	antlon:"",
	finlat:"",
	finlon:"",
	distancia:0,
	minutos:0,
	total:0,
	so:"",
	estado:""
},
Tarifa={
	"paquete":"RC5PVIP",
	"origen":"Bogotá",
	"destino":"Bogotá",
	"unidadseg":60,
	"tarifaundseg":200,
	"unidadkm":1,
	"tarifaundkm":1200,
	"minima":6000,
	"comienzaen":5000
};

exports.setServicioIni = function(datos,id,vehiculo){
	logsCarrera.push("Nuevo Servicio");
	Ti.App.Properties.setList('logsCarrera',logsCarrera);
	Servicio.paquete=datos.paquete,
	Servicio.servicioid=datos.servicio,
	Servicio.room=datos.room,
	Servicio.fpago=datos.fpago.id,
	Servicio.fpagonom=datos.fpago.nombre,
	Servicio.pasajeroid=datos.idpasajero;
	Servicio.conductorid=id;
	Servicio.tiqueteid=datos.tiqueteid;
	Servicio.tiquete=datos.tiquete;
	Servicio.vehiculo=vehiculo;
	Servicio.pushid=datos.key;
	Servicio.so=datos.so;
	Servicio.token="";
	Servicio.estado="TOMADO";
	if(datos.fpago.id==4){
		Servicio.token=datos.fpago.token;
	}
	Ti.App.Properties.setList('Servicio',Servicio);
	
};
exports.updateEstado = function(estado){
	Servicio.estado=estado;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.getEstado = function(){
	return Servicio.estado;
};
exports.getTiquete = function(){
	return Servicio.tiquete;
};
exports.setMtarifa = function(mtarifa,callback){
	Servicio.muestratarifa=mtarifa;
	Ti.App.Properties.setList('Servicio',Servicio);
	callback("ok");
};
exports.getServicioRoom = function(){
	return Servicio.room;
};
exports.setServicioRoom = function(room){
	Servicio.room=room;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.setFpago = function(fpago){
	Servicio.fpago=fpago;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.setPaquete = function(paquete){
	Servicio.paquete=paquete;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.getPaquete = function(){
	return Servicio.paquete;
};
exports.setServicioid = function(id){
	Servicio.servicioid=id;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.getServicioid = function(){
	return Servicio.servicioid;
};
exports.getServiciofpago = function(){
	return {"nombre":Servicio.fpagonom,"id":Servicio.fpago};
};
exports.setTarifa = function(tarifa){
	Tarifa=tarifa;
	Servicio.tarifaid=tarifa.id;
	Servicio.origen=tarifa.origen;
	Servicio.destino=tarifa.destino;
	Ti.App.Properties.setList('Servicio',Servicio);
	Ti.App.Properties.setList('Tarifa',Tarifa);
};
exports.getTarifa = function(){
	return Tarifa;
};
exports.setServicio = function(coor,horaini,fechaini,fecha){
	
	Servicio.inilat=coor.latitude;
	Servicio.inilon=coor.longitude;
	Servicio.horaini=horaini;
	Servicio.fechaini=fechaini;
	Servicio.fecha=fecha;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.setAntCoor = function(coor){
	Servicio.antlat=coor.latitude;
	Servicio.antlon=coor.longitude;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.setDistancia = function(distancia){
	Servicio.distancia=distancia;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.getDistancia = function(){
	return Servicio.distancia;
};
exports.setTiempo = function(minutos){
	Servicio.minutos=minutos;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.getTiempo = function(){
	return Servicio.minutos;
};
exports.getFechaini = function(){
	return Servicio.fechaini;
};
exports.getAntCoor = function(){
	return {latitude:Servicio.antlat,longitude:Servicio.antlon};
};
exports.setFinServicio = function(coor,distancia,tiempo,horafin){
	Servicio.finlat=coor.latitude;
	Servicio.finlon=coor.longitude;
	Servicio.distancia=distancia;
	Servicio.minutos=tiempo;
	Servicio.horafin=horafin;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.getPrecio = function(final){
	var distancia=Servicio.distancia*Tarifa.tarifaundkm;
	var tiempo=Servicio.minutos*Tarifa.tarifaundseg;
	var total=distancia+tiempo+parseInt(Tarifa.comienzaen);
	if(final==1 && total<Tarifa.minima){
		total = Tarifa.minima;
	}

	return Math.round(total/50)*50;
};
exports.getPrecioSave = function(reg){
	var distancia=reg.servicio.distancia*Tarifa.tarifaundkm;
	var tiempo=reg.servicio.minutos*reg.tarifa.tarifaundseg;
	var total=distancia+tiempo+parseInt(reg.tarifa.comienzaen);
	if(total<reg.tarifa.minima){
		total = Tarifa.minima;
	}

	return Math.round(total/50)*50;
};
exports.getServicioUdimetro = function(){
	return Servicio;
};
exports.sincronizar = function(opc,callbak){
	if(opc==1){
		//clearInterval(SyncFuec);
		
		var reg={servicio:Servicio,tarifa:Tarifa};
		ServSync.push(reg);
		Ti.App.Properties.setList('ServSync',ServSync);
		callbak("ok");
		logsCarrera.push("fin de carrera dice conductor");
	    Ti.App.Properties.setList('logsCarrera',logsCarrera);
	}
	if(ServSync.length>0){  
		
		if(ServSync[0].servicio.fuecsid!=""){
			exports.guardar();
		}
		if(ServSync[0].servicio.inilat=="" && ServSync[0].servicio.origen==""  && ServSync[0].servicio.destino==""){
    			ServSync.shift();
    	}
        if(ServSync[0].servicio.inilat!="" && ServSync[0].servicio.paquete!=""  && ServSync[0].servicio.pasajeroid!="" && ServSync[0].servicio.fuecsid==""){
			exports.getFuec(ServSync[0].servicio,false);
		}
		
	}
	
	Sincronizar = setInterval(function(){
		//Ti.API.info(" ServSync= "+JSON.stringify(ServSync));
		if(ServSync.length>0){  
			logsCarrera.push("sincronizando carreras no enviadas");
	    Ti.App.Properties.setList('logsCarrera',logsCarrera);		
			if(ServSync[0].servicio.fuecsid!=""){
			exports.guardar();
			}
			if(ServSync[0].servicio.inilat=="" && ServSync[0].servicio.paquete==""  && ServSync[0].servicio.pasajeroid==""){
	    			ServSync.shift();
	    	}
	        if(ServSync[0].servicio.inilat!="" && ServSync[0].servicio.paquete!=""  && ServSync[0].servicio.pasajeroid!="" && ServSync[0].servicio.fuecsid==""){
				exports.getFuec(ServSync[0].servicio,false);
			}
		}else{
			clearInterval(Sincronizar);
		}
	}, 30000);
};
exports.mostrarFuec = function(){
	 //Ti.API.info("SE VA A MOSTRAR EL FUEC = "); 
	sincronizandofuec=true;
	exports.getFuec(Servicio,true);
	SyncFuec = setInterval(function(){
		if(Servicio.fuecsid==""){
			if(sincronizandofuec===false){
				sincronizandofuec=true;
				exports.getFuec(Servicio,true);
			}
			
			
		}else{
			clearInterval(SyncFuec);
		}
	}, 60000);
};
exports.sincronizarAbordo = function(opc){
	    if(opc==1){
	    	tiqueteSync.push(Servicio.tiqueteid);
	        Ti.App.Properties.setList('tiqueteSync',tiqueteSync);
	       }
	    SyncAbordar = setInterval(function(){
		if(tiqueteSync.length>0){
			exports.restaTiquete();
		}else{
			clearInterval(SyncAbordar);
		}
	}, 30000);
};
exports.guardar = function(){
	         logsCarrera.push("vamos a guardar carrera en servidor");
	    Ti.App.Properties.setList('logsCarrera',logsCarrera);
    	        var xhr=Titanium.Network.createHTTPClient();    
    		xhr.open('POST',Alloy.Globals.server.api+'v1/carreras'); 	
    		xhr.onload = function(e){
    			var data = JSON.parse(this.responseText);
                //Ti.API.info("La respuesta de intelligent es = "+JSON.stringify(data));  
    			 if (this.status == 201) {
    			 	logsCarrera.push("carrera guardada ");
	    Ti.App.Properties.setList('logsCarrera',logsCarrera);						
					ServSync.shift();
					Ti.App.Properties.setList('ServSync',ServSync);
					//Ti.API.info("La informacion de ServSync despues de guardar = "+JSON.stringify(ServSync)); 
					if(ServSync.length==0){
						clearInterval(Sincronizar);
						Ti.App.Properties.setString('Sincronizado',true);
						Sincronizado=true;
					}
                } else {
                	logsCarrera.push("estatus diferente a 201");
	    			Ti.App.Properties.setList('logsCarrera',logsCarrera);            	
                	Sincronizado=false;
                	Ti.App.Properties.setString('Sincronizado',false);
                }
	     		
	                 
   		};
    	xhr.onerror = function(e){
    		//alert('Transmission error: ' + e.error);
    		logsCarrera.push("Fallo la guardada de la carrera y se va a pasar a lista de no sincronizados");
	        Ti.App.Properties.setList('logsCarrera',logsCarrera);
    		 //Ti.API.info("CARRERA QUE NO SE PUEDE SINCRONIZAR = "+JSON.stringify(ServSync[0])); 
    		 ServErrorSync.push(ServSync[0]);
    		 Ti.App.Properties.setList('ServErrorSync',ServErrorSync);
    		 ServSync.shift();
    		Sincronizado=false;
            Ti.App.Properties.setString('Sincronizado',false);
    		};
    	xhr.setRequestHeader("Content-Type","application/json ");
    	xhr.setRequestHeader("Accept","application/json");
    	var array={
					latini: ServSync[0].servicio.inilat,
					longini:ServSync[0].servicio.inilon,
					latfin: ServSync[0].servicio.finlat,
					longfin: ServSync[0].servicio.finlon,
					origen: ServSync[0].servicio.origen,
					destino: ServSync[0].servicio.destino,
					horaini: ServSync[0].servicio.horaini,
					horafin:ServSync[0].servicio.horafin,
					fecha:ServSync[0].servicio.fecha,
					distancia:ServSync[0].servicio.distancia.toFixed(2),
					tiempo:ServSync[0].servicio.minutos,
					precio:exports.getPrecioSave(ServSync[0]),
					so:ServSync[0].servicio.so,
					imei:Titanium.Platform.id,
					paquete:ServSync[0].servicio.paquete,
					pasajero:ServSync[0].servicio.pasajeroid,
					fpago:ServSync[0].servicio.fpago,
					token:ServSync[0].servicio.token,
					tiquete:ServSync[0].servicio.tiqueteid,
					tiquetecod:ServSync[0].servicio.tiquete,
					conductor:ServSync[0].servicio.conductorid,
					fuec:ServSync[0].servicio.fuecsid,
					vehiculo:ServSync[0].servicio.vehiculo,
					key: ServSync[0].servicio.pushid,
					muestratarifa: ServSync[0].servicio.muestratarifa,
    				};
    				 //Ti.API.info("la carrera a enviar es a enviar  = "+JSON.stringify(array)); 
    	xhr.send(JSON.stringify(array));     			

		};
exports.setFuec = function(fuec,archivo){
	Servicio.fuecsid=fuec;
	Servicio.fuecurl=archivo;
	Ti.App.Properties.setList('Servicio',Servicio);
};
exports.getArchivo = function(){
	return Servicio.fuecurl;
};
exports.getFuec = function(fuecdata,mostrar){
	if(fuecdata.origen!="" && fuecdata.destino!="" && fuecdata.pasajeroid!="" && fuecdata.conductorid!="" && fuecdata.vehiculo!="" && fuecdata.paquete!="" && fuecdata.fuecsid==""){
		    logsCarrera.push("Vamos a crear fuec");
	        Ti.App.Properties.setList('logsCarrera',logsCarrera);
		var xhr=Titanium.Network.createHTTPClient();    
    		xhr.open('GET',Alloy.Globals.server.app+'/Presentacion/ContratoPresentacion.php'); 	
    		xhr.onload = function(e){
    			var data = JSON.parse(this.responseText);
    			 
    			 if(data.resultado=="true"){
    			 	
					if(mostrar===true){
						//Ti.API.info("la creacion del fuec durante el servicio = "+JSON.stringify(data)); 
						Servicio.fuecsid=data.fuec;
						Servicio.fuecurl=data.archivo;
						Ti.App.Properties.setList('Servicio',Servicio);
						clearInterval(SyncFuec);
						sincronizandofuec===false;
						Ti.App.fireEvent("servicio:setfuec");
						logsCarrera.push("fuec creado durante el servicio fuec= "+data.fuec);
	                    Ti.App.Properties.setList('logsCarrera',logsCarrera);
							
					}else{
						//Ti.API.info("la creacion del fuec  en ServSync= "+JSON.stringify(data)); 
						logsCarrera.push("fuec creado desde ServSync fuec= "+data.fuec);
	                    Ti.App.Properties.setList('logsCarrera',logsCarrera);
						if(ServSync[0].servicio){
							ServSync[0].servicio.fuecsid=data.fuec;
							ServSync[0].servicio.fuecurl=data.archivo;
							Ti.App.Properties.setList('ServSync',ServSync);
						}
						
					}	
                  }
                          
   		};
    	xhr.onerror = function(e){
    		//alert('Transmission error: ' + e.error);
    		logsCarrera.push("No se pudo crear fuec por error de transmision = ");
	        Ti.App.Properties.setList('logsCarrera',logsCarrera);
	        sincronizandofuec===false;
    		};
    	xhr.setRequestHeader("Content-Type","application/json ");
    	xhr.setRequestHeader("Accept","application/json");
    	var array={
						origen: fuecdata.origen,
						destino:fuecdata.destino,
						pasajero: fuecdata.pasajeroid,
						conductor: fuecdata.conductorid,
						vehiculo: fuecdata.vehiculo,
						paquete: fuecdata.paquete
    				};
    	var datos={
    				modulo:"getContrato",
    				datos:JSON.stringify(array),
    				};

    	xhr.send(datos); 
 }   	
    	   
};
exports.restaTiquete = function(){
		var xhr=Titanium.Network.createHTTPClient();    
    		xhr.open('PUT',Alloy.Globals.server.api+'v1/tiquetesrestas/'+tiqueteSync[0]); 	
    		xhr.onload = function(e){
    			var data = JSON.parse(this.responseText);
                //Ti.API.info("La respuesta de intelligent es = "+JSON.stringify(data));  
    			 if (this.status == 204) {	
    			 	//Ti.API.info("PERFECTO SE RESTO EL TIQUETE = "+tiqueteSync[0]);
    			 	 tiqueteSync.shift();
					 Ti.App.Properties.setList('tiqueteSync',tiqueteSync);		
					 
                } else {            	
                	//Ti.API.info("JUEMADRE NO SE RESTO EL TIQUETE = "+tiqueteSync[0]);
                }
	     		
	                 
   		};
    	xhr.onerror = function(e){
    		//alert('Transmission error: ' + e.error);
    		};
    	xhr.setRequestHeader("Content-Type","application/json ");
    	xhr.setRequestHeader("Accept","application/json");
    	xhr.send();     
};
exports.informarLlegada =function(){
		var xhr=Titanium.Network.createHTTPClient();   
		Ti.API.info("direccion para enviar mensaje = "+Alloy.Globals.server.api+'v1/mensajesllegadas'); 
    		xhr.open('POST',Alloy.Globals.server.api+'v1/mensajesllegadas'); 	
    		xhr.onload = function(e){
    			var data = JSON.parse(this.responseText);  
    			 if (this.status == 201) {			
                } else {            	
                	Ti.API.info("JUEMADRE NO SE ENVIO EL MENSAJE = ");
                }
	     		
	                 
   		};
    	xhr.onerror = function(e){
    		//alert('Transmission error: ' + e.error);
    		};
    	xhr.setRequestHeader("Content-Type","application/json ");
    	xhr.setRequestHeader("Accept","application/json");
    	var array={
					key: Servicio.pushid,
					so:Servicio.so,
    				};
    	Ti.API.info("datos push = "+JSON.stringify(array));
    	xhr.send(JSON.stringify(array)); 
};
exports.informarAbordo =function(){
		var xhr=Titanium.Network.createHTTPClient();    
    		xhr.open('POST',Alloy.Globals.server.api+'v1/mensajesabordajes'); 	
    		xhr.onload = function(e){
    			var data = JSON.parse(this.responseText);
    			 if (this.status == 201) {			
                } else {            	
                	//Ti.API.info("JUEMADRE NO SE ENVIO EL MENSAJE = ");
                }
	     		
	                 
   		};
    	xhr.onerror = function(e){
    		//alert('Transmission error: ' + e.error);
    		};
    	xhr.setRequestHeader("Content-Type","application/json ");
    	xhr.setRequestHeader("Accept","application/json");
    	var array={
					key: Servicio.pushid,
					so:Servicio.so,
    				};
    	xhr.send(JSON.stringify(array)); 
};
exports.limpiaDatos = function(){
	Servicio={
	conductorid:"",
	vehiculo:"",
	pasajeroid:"",
	servicioid:"",
	tiqueteid:"",
	tarifasid:"",
	fuecsid:"",
	estado:"",
	fuecurl:"",
	paquete:"",
	room:"",
	tiquete:"",
	fpago:"",
	fpagonom:"",
	token:"",
	pushid:"",
	muestratarifa:"",
	origen:"",
	destino:"",
	inilat:"",
	inilon:"",
	fecha:"",
	horaini:"",
	horafin:"",
	antlat:"",
	antlon:"",
	finlat:"",
	finlon:"",
	so:"",
	distancia:0,
	minutos:0,
	total:0,
};
 Ti.App.Properties.setList('Servicio',Servicio);

};
/*
 * Cada vez que el conductor ingresa a la aplicacion recupera los datos anteriores
 */ 
exports.recoverConf = function(){
	Conf=Ti.App.Properties.getList('Conf');
	Servicio=Ti.App.Properties.getList('Servicio');
	Pasajero=Ti.App.Properties.getList('Pasajero');
	Tarifa=Ti.App.Properties.getList('Tarifa');
};
if(Ti.App.Properties.hasProperty('Servicio')){
		 Servicio=Ti.App.Properties.getList('Servicio');
		 //Ti.API.info("SERVICIO ACTUAL = "+JSON.stringify(Servicio) );
		 
	}
if(Ti.App.Properties.hasProperty('Tarifa')){
		 Tarifa=Ti.App.Properties.getList('Tarifa');
	}
if(Ti.App.Properties.hasProperty('ServSync')){
		ServSync=Ti.App.Properties.getList('ServSync');
		//Ti.API.info("SERVICIO PARA SINCRONIZAR ES = "+JSON.stringify(Ti.App.Properties.getList('ServSync')) );
	}
if(Ti.App.Properties.hasProperty('ServErrorSync')){
		ServErrorSync=Ti.App.Properties.getList('ServErrorSync');
		//Ti.API.info("SERVICIO CON ERRORES SIN SINCRONIZAR = "+JSON.stringify(Ti.App.Properties.getList('ServErrorSync')) );
	}
if(Ti.App.Properties.hasProperty('logsCarrera')){
	    //Ti.App.Properties.setList('logsCarrera',logsCarrera);
		logsCarrera=Ti.App.Properties.getList('logsCarrera');
		//Ti.API.info("EL REGISTRO DE LOGS ES  = "+JSON.stringify(Ti.App.Properties.getList('logsCarrera')) );
}
if(Ti.App.Properties.hasProperty('Sincronizado')){
		Sincronizado=Ti.App.Properties.getString('Sincronizado');
		//Ti.API.info("ESTADO DE SINCRONIZACION DE CARRERAS = "+JSON.stringify(Ti.App.Properties.getString('Sincronizado')) );
		if(ServSync.length>0){
			exports.sincronizar(0,function(resp){
				
			});
		}
	}
if( Ti.App.Properties.hasProperty('tiqueteSync') ){
	    	//Ti.API.info("ESTADO DE SINCRONIZACION DE TIQUETE = "+JSON.stringify(Ti.App.Properties.getString('tiqueteSync')) );
			exports.sincronizarAbordo(0);

		
	}
