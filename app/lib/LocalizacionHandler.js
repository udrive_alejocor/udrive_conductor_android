var departamentos=["CUNDINAMARCA","VALLE DEL CAUCA","META","ANTIOQUIA","ATLÁNTICO","BOLÍVAR"];
var Origen;
if(Ti.App.Properties.hasProperty('Origen')){
		 Origen=Ti.App.Properties.getString('Origen');
	}
exports.getOrigen=function(callback){
		    //Ti.API.info("////////Empezando getPosicionDatos");
			Ti.Geolocation.getCurrentPosition(function(e) {
			var ciudad = "";
			if(!isNaN(parseFloat(e.coords.latitude)) && isFinite(e.coords.latitude) && !isNaN(parseFloat(e.coords.longitude)) && isFinite(e.coords.longitude)){
				reverseApi(function(resp){
						if(resp.respuesta=="OK"){
							Origen=resp.origen.replace(" ", "");
							Ti.App.Properties.setString('Origen',resp.origen.replace(" ", ""));
							callback({
								ciudad : resp.origen.replace(" ", ""),
							});
						}
					},e.coords.latitude, e.coords.longitude);
			

			}
		});
	};
function reverseApi(callback,latitude,longitude){
			var xhr=Titanium.Network.createHTTPClient();    
    		xhr.open('GET','http://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude); 	
    		xhr.onload = function(e){
    			
                ////Ti.API.info("La respuesta con lat="+latitude+" y long="+longitude+"de google es = "+JSON.stringify(data)); 
    			 if (this.status == 200) {	
                    var data = JSON.parse(this.responseText);
    			 	if(data.results[0]!=undefined){
    			 	////Ti.API.info("La direccion con la que trabajo es  = "+JSON.stringify(data.results[0].formatted_address));
    			 	var dir_array = data.results[0].formatted_address.split(","); 	
    			 	var posbog = dir_array[dir_array.length - 2].toUpperCase();
    			 	
    			 	var ciudad="";
						var a = departamentos.indexOf(posbog.replace(" ", ""));
						if(a>=0){
							ciudad = dir_array[dir_array.length - 3].toUpperCase();							
						}else{
							ciudad=posbog;
						}	
					callback({respuesta:data.status,origen:ciudad}); 
					}
                } 
	     		
	                 
   		};
    	xhr.onerror = function(e){
    		//alert('Transmission error: ' + e.error);
    		};
    	xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    	xhr.setRequestHeader("Accept","application/json");
    	xhr.send();  
}