var io = require('socket.io');
var CondHandler = require('ConductorHandler');
var ServHandler = require('ServicioHandler');
var PaqHandler = require('PaqueteHandler');

//Estas variables están disponibles en el módulo, pero no fuera de él
var conductor;
var servicio;
var winservicio;
var actwindow;
var dir;
var pasajero;
var  repetirvar;
var recaudo;
var distanciapas;
var bloqueado=false;
var online = true;
var offline = null;
var policiaConect="";
var myVer = Ti.App.Android.appVersionCode;
//var socket = io.connect('http://54.85.75.141:82/driver_channel', {
var socket = io.connect(Alloy.Globals.server.socket, {
	'reconnect' : true,
	'reconnection delay' : 10000,
	'max reconnection attempts' : 100000
});
socket.on('connect', function() {
	Ti.API.log('connected!');
	clearInterval(offline);
	offline=null;
	online = true;

});
socket.on('identificate', function() {
	if (online == "true" || online == true) {
		
		switch(CondHandler.getEstado()) {
		case 'ENCAMINO':
			if (online === true) {
			acciones.getPosicionDatos(function(response) {
				response.room=ServHandler.getServicioRoom();
				response.version=myVer;
				//Ti.API.info("ESTOY EN CAMINO = " + JSON.stringify(response));
				socket.emit('recuperarCarrera', response);
			});
			Ti.App.fireEvent("servicio:startposVehiculo");
			}
			break;
		case 'LIBRE':
		   bloqueado=false;
		   if (online === true) {
			acciones.getPosicionDatos(function(response) {
				
				response.version=myVer;
				Ti.API.info("ESTOY LIBRE = " + JSON.stringify(response));
				socket.emit('soyConductor', response, 
                  function(confirmation){
                          // send data
                          // know we got it once the server calls this callback      
                          // note -in this ex we dont need to send back any data 
                          // - could just have called fn() at server side
                          //Ti.API.info("La confirmacion de entregado = " + confirmation);
                  });
			});
			}
			break;
		case 'ENCARRERA':
			if (online === true) {
				acciones.getPosicionDatos(function(response) {
					response.room=ServHandler.getServicioRoom();
					response.version=myVer;
					//Ti.API.info("ESTOY EN CARRERA = " + JSON.stringify(response));
					socket.emit('recuperarCarrera', response);
				});
			}
			break;
		}
	}

});
socket.on('disconnect', function() {
	Ti.App.fireEvent("servicio:stopposVehiculo");
	online = false;
	offline = setInterval(reconectar, 5000);
	Ti.App.fireEvent("servicio:estado", {
		"texto" : "Desconectado",
		"color":"#B2B1B0"
	});
	

});
socket.on('connect_failed', function() {
	online = false;
	if(offline===null){
		offline = setInterval(reconectar, 5000);
		Ti.App.fireEvent("servicio:estado", {
			"texto" : "Desconectado"
		});
	}
	
});
socket.on('mensajeServidor', function(response) {
	//Ti.API.info("El servidor dice = " + response);
	Ti.App.fireEvent("servicio:estado", {
		"texto" : ""+response,
		"color":"#47A1FC"
	});
	 
});
socket.on('alertaServidor', function(response) {
	Ti.App.fireEvent("open:servicio",{accion:"chat"});
	Ti.App.fireEvent("servicio:alerta",{texto:response});
});
socket.on('sincronizarPaquetes', function(response) {
	Ti.App.fireEvent("sincronizar:paquetes");
});
socket.on('carreraRegistrada', function(response) {
	setSincronizacion('CarreraDatos', 'update1receive', 'si');
	Ti.App.Properties.setString('carreraId', response);
});
socket.on('actArea', function(response) {

	Ti.App.Properties.setString('Area', response);
});

socket.on('nuevoServicio', function(response) {
	if ((online == "true" || online === true) && CondHandler.getEstado() == "LIBRE" && bloqueado===false) {
		bloqueado=true;
		servicio = response;
		Ti.App.Properties.setList('pasajerodat', response);
		Ti.API.info("informacion del servicio = " + JSON.stringify(response));
		Ti.Geolocation.getCurrentPosition(updatePosition);
	}
});
socket.on('activartarifario', function(response) {
	Ti.App.Properties.setString('ciudadini', response);
	if (online == "true" || online == true) {
		actualizartarifas(response);
	}
});
socket.on('terminoCarrera', function(response) {
	Ti.App.Properties.setList('CarreraDatos', {});
	//Ti.App.fireEvent("servicio:terminar");
});
socket.on('servicioTomado', function() {
	clearInterval(policiaConect);
	ServHandler.updateEstado("CONFIRMADO");
	Ti.App.fireEvent("servicio:tomado");
	//Ti.API.info("EL SERVIDOR DICE QUE YA TOMO EL SERVICIO");
	Ti.App.fireEvent("servicio:estado", {
		"texto" : "Conectado EN SERVICIO",
		"color":"#47A1FC"
	});
});
socket.on('canceladook', function() {
	clearInterval(policiaConect);
	CondHandler.setEstado("LIBRE");
	ServHandler.updateEstado("CANCELADO");
	Ti.App.fireEvent("servicio:cancelado");
	//Ti.API.info("EL SERVIDOR DICE QUE YA CANCELO SERVICIO");
});
socket.on('finCarreraOk', function(response) {
	//Ti.API.info("LISTO CONFIRMADO  = ");
	CondHandler.setEstado("LIBRE");
	ServHandler.updateEstado("TERMINADO");
	clearInterval(policiaConect);
	//Ti.API.info("El servidor dice = " + response);
	Ti.App.fireEvent("servicio:estado", {
		"texto" : ""+response,
		"color":"#47A1FC"
	});
});
socket.on('clienteCancela', function() {
	if (CondHandler.getEstado() == "ENCAMINO") {
		Ti.App.fireEvent("open:servicio",{accion:"cancelado"});
		ServHandler.updateEstado("CANCELADO");
		CondHandler.setEstado("LIBRE");
		//Ti.Media.vibrate();
		alert("El cliente ha cancelado el servicio");
		Ti.App.fireEvent("cliente:cancela");
		if (online == "true" || online == true) {
			acciones.getPosicionDatos(function(response) {
				response.room=Ti.App.Properties.getList('pasajerodat').room;
				response.version=myVer;
				//Ti.API.info("informacion para REINGRESAR = " + JSON.stringify(response)+" room "+Ti.App.Properties.getList('pasajerodat').room);
				socket.emit('Reingresar', response);
			});
		}
	}

});
socket.on('servicioRechazado', function(response) {
	CondHandler.setEstado("LIBRE");
	Ti.Media.vibrate();
	Ti.App.fireEvent("servicio:rechazado", {
		"resp" : response
	});

});
socket.on('mensajeChat', function (response) {
	var men={mensaje:response};
	Ti.Media.vibrate();
	//Ti.API.info("el mensaje es  = " + response);
	Ti.App.fireEvent("open:servicio",{accion:"chat"});
	Ti.App.fireEvent("chat:nuevomensaje",men);
	
});
exports.activarLibre = function() {
	CondHandler.setEstado("LIBRE");
	if (online == "true" || online == true) {
		acciones.getPosicionDatos(function(response) {
			response.version=myVer;
				//Ti.API.info("informacion para registrar = " + JSON.stringify(response));
				socket.emit('soyConductor', response);
			});
	}

};
exports.cambioPlaca = function() {
	if ((online == "true" || online == true) && CondHandler.getEstado()=="LIBRE" ) {
		acciones.getPosicionDatos(function(response) {
			response.version=myVer;
				//Ti.API.info("informacion cambiar placa = " + JSON.stringify(response));
				socket.emit('cambioPlaca', response);
			});
	}

};
exports.tomarServicio = function() {
	sincronizar();
	CondHandler.setEstado("ENCAMINO");
	if (online == "true" || online == true) {	
	 clearInterval(repetirvar);
	 bloqueado=false;
	ServHandler.setServicioIni(Ti.App.Properties.getList('pasajerodat'),CondHandler.getConductor().id,CondHandler.getConductor().vehiculoact.id);
	acciones.getPosicionDatos(function(response) {
				//Ti.API.info("informacion para enviarle al pasajero = " + JSON.stringify(response));
				socket.emit('tomarServicio', {
				llegaen : "10min",
				placa : CondHandler.getConductor().placaact,
				documento : CondHandler.getConductor().documento,
				imei : CondHandler.getConductor().imei,
				room : Ti.App.Properties.getList('pasajerodat').room,
				latitud:response.latitud,
				longitud:response.longitud,
				ciudad:response.ciudad,
				paquetes:response.paquetes,
				vehiculo:CondHandler.getVehiculo().nombre
			}, 
             function(confirmation){
                          //Ti.API.info("El PASAJERO RECIBIO LA  = " + confirmation);
                  });
			});
	
		}

};
exports.enviarChat=function(mensaje){
	//Ti.API.info("El mensaje desde sockw "+mensaje);
	socket.emit('enviarChat', {
				mensaje : mensaje,
				room : ServHandler.getServicioRoom()
			});
};
exports.cancelarServicio = function(room) {
	sincronizar();
	ServHandler.updateEstado("CONDCANCELO");
	if (online == "true" || online == true) {
	//Ti.API.info("el room es  = " +room);
	if (CondHandler.getEstado() == "ENCAMINO") {
		
		if (online == "true" || online == true) {
			acciones.getPosicionDatos(function(response) {
				response.version=myVer;
				response.room=room;
				//Ti.API.info("informacion para comenzar nuevamente = " + JSON.stringify(response));
				socket.emit('cancelaCarrera', response);
			});
		}
		
	}
	}
};
exports.sendPostoCliente=function(geo){
	//Ti.API.info("la posicion del vehiculo que esta en camino es = " + JSON.stringify(geo));
	socket.emit('posVehiculo', {
				posicion : geo.coords,
				room : ServHandler.getServicioRoom()
			});
	
};
exports.infoLlegada = function() {
	var sala = Ti.App.Properties.getString('Room');
	var datoscond = Ti.App.Properties.getList('conductordat');
	var datos = {
		placa : datoscond.placa,
		imei : datoscond.imei,
		area : Ti.App.Properties.getString('Area'),
		room : sala
	};
	socket.emit('llegue', datos);

};
exports.finCarrera = function(muestratarifa,precio,room) {
	
		acciones.getPosicionDatos(function(response) {
			response.version=myVer;
			response.precio=precio;
			response.room=room;
				response.muestratarifa=muestratarifa;
				//Ti.API.info("informacion para registrar = " + JSON.stringify(response));
				socket.emit('finCarrera', response);
			sincronizar();
			});
		


};
exports.setwin = function(window) {

	actwindow = window;

};
exports.setdir = function(dire) {

	dir = dire;

};
exports.setpasajero = function(pasaj) {

	pasajero = pasaj;

};
exports.setventana = function(window) {

	winservicio = window;

};
exports.setrecaudo = function(fpago) {

	recaudo = fpago;

};
exports.setdistanciapas = function(distancia) {

	distanciapas = distancia;

};
exports.abordoCliente =function(){
	socket.emit('abordo', {
				room : ServHandler.getServicioRoom()
			});
};
exports.desbloquear =function(){
	bloqueado=false;
};
function updatePosition(e) {
	var unit = "K";
	if (!e.success || e.error) {
		alert("No podemos ubicarte.");
		Ti.API.debug(JSON.stringify(e));
		Ti.API.debug(e);
		return;
	}
	var radlat1 = Math.PI * e.coords.latitude / 180;
	var radlat2 = Math.PI * servicio.latitud / 180;
	var radlon1 = Math.PI * e.coords.longitude / 180;
	var radlon2 = Math.PI * servicio.longitud / 180;
	var theta = e.coords.longitude - servicio.longitud;
	var radtheta = Math.PI * theta / 180;
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist);
	dist = dist * 180 / Math.PI;
	dist = dist * 60 * 1.1515;
	if (unit == "K") {
		dist = dist * 1.609344;
	}
	if (unit == "N") {
		dist = dist * 0.8684;
	}
	if (dist <= 2) {
		if(winservicio.getVisible()===true || winservicio.getVisible()=="true"){
	
		}else{
			
			
			data = Ti.App.Properties.getList('pasajerodat');
			dir.setText(data.direccion);
			pasajero.setText(data.nombre);
			recaudo.setText("Pago con "+data.fpago.nombre);
			distanciapas.setText(dist);
			//
			//Ti.Media.vibrate();
			
			clearInterval(repetirvar);
			Ti.App.fireEvent("open:servicio",{accion:"nuevo"});
			winservicio.visible=true;
			repetirvar=setInterval(function(){ 
		          winservicio.visible=false;
		          clearInterval(repetirvar);
		          bloqueado=false;
			},10000);
		    
		}
			

		
	}
};
function dondeEstoy(callback) {
	Ti.Geolocation.getCurrentPosition(function(e) {
		var ciudad = "";
		if(!isNaN(parseFloat(e.coords.latitude)) && isFinite(e.coords.latitude) && !isNaN(parseFloat(e.coords.longitude)) && isFinite(e.coords.longitude)){
		Ti.Geolocation.reverseGeocoder(e.coords.latitude, e.coords.longitude, function(evt) {
			if (evt.code != -1) {
				places = evt.places[0];
				var dir_array = places.address.split(",");
				var posbog = dir_array[dir_array.length - 3];
				//Ti.API.info("la ciudad es " + posbog);
				if (posbog.match(/bogot/gi) != "" && posbog.match(/bogot/gi) != null) {
					ciudad = "Bogotá";
				} else {
					ciudad = dir_array[dir_array.length - 4];
				}
				var datoscond = Ti.App.Properties.getList('conductordat');
				var room = Ti.App.Properties.getString('Room');
				var areacond = Ti.App.Properties.getString('Area');
				callback({
					placa : datoscond.placa,
					documento : datoscond.documento,
					imei : datoscond.imei,
					latitud : e.coords.latitude,
					longitud : e.coords.longitude,
					servicio : datoscond.servicioid,
					room : room,
					area : areacond,
					ciudad : ciudad.replace(" ", ""),
					estado : Ti.App.Properties.getString('estConductor')
				});
			}

		});
		}
	});

}




function sincronizar() {
	policiaConect=setInterval(function(){
	//Ti.API.info("VA A OBSERVAR EL POLICIA");
     if (online === true) {
     	//Ti.API.info("EL ESTADO DEL CONDUCTOR ES "+CondHandler.getEstado());
     	//Ti.API.info("EL ESTADO DEL SERVICIOS ES "+ServHandler.getEstado());
		switch(CondHandler.getEstado()){
			case 'ENCAMINO':
				/*
				 * Debemos validar que este verificado el tomado y que el pasajero no haya cancelado el servicio
				 */
				  switch(ServHandler.getEstado()){
					  case 'TOMADO':
					           //El conductor tomo el servicio pero el servidor aun no le confirma si quedo realmente registrado
					            //Ti.API.info("ESTA EN CAMINO CAMINO Y VA A SOLICITAR CONFIRMACION");
							  	acciones.getPosicionDatos(function(response) {
								//Ti.API.info("informacion para enviarle al pasajero = " + JSON.stringify(response));
								socket.emit('tomarServicio', {
								llegaen : "10min",
								placa : CondHandler.getConductor().placaact,
								documento : CondHandler.getConductor().documento,
								imei : CondHandler.getConductor().imei,
								room : Ti.App.Properties.getList('pasajerodat').room,
								latitud:response.latitud,
								longitud:response.longitud,
								ciudad:response.ciudad,
								vehiculo:CondHandler.getVehiculo().nombre
								}, 
					             function(confirmation){
					                          //Ti.API.info("El PASAJERO RECIBIO LA  = " + confirmation);
					                  });
								}); 	
						break;
						case 'CONFIRMADO':
						 //Ti.API.info("ESTA CONFIRMADO Y VA A REVISAR QUE NO HAYA CANCELADO");
						//El servidor confirmo que ya el conductor tomo el servicio, pero necesitamos vigilar pos si cancela en el camino
							   socket.emit('verificarEstados', {
							   	estado:'CONFIRMADO',
								placa : CondHandler.getConductor().placaact,
								documento : CondHandler.getConductor().documento,
								room : Ti.App.Properties.getList('pasajerodat').room,
								});
						break;
						case 'CONDCANCELO':
							acciones.getPosicionDatos(function(response) {
							response.version=myVer;
							response.room=ServHandler.getServicioRoom();
							response.estado='CONDCANCELO';
							socket.emit('verificarEstados', response);
						});
						break;
					}
			break;
		case 'LIBRE':
		 	/*
			 * Debemos validar que el conductor este activo en los paquetes asignados
			 */
			 	if (socket.socket.connected === false) {
			 		reconectar();
			 	}
			break;
		case 'ENCARRERA':
			 /*
			  * Debemos validar que el conductor este en carrera
			  */
			   if(ServHandler.getEstado()==""){
			   	  //Ti.API.info("El CONDUCTOR NO HA RECIBIDO LA CONFIRMACION  = ");
					 acciones.getPosicionDatos(function(response) {
					 	response.version=myVer;
					 	response.estado='SERVTERMINADO';
						response.room=Ti.App.Properties.getList('pasajerodat').room,
					socket.emit('verificarEstados', response);
					});
			   }
		break;
		}
		}
	}, 10000);
	
}



function reconectar() {
	Ti.App.fireEvent("servicio:estado", {
		"texto" : "Conectando..."
	});
	Ti.App.fireEvent("servicio:stopposVehiculo");
	if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {
		Ti.API.log('No hay rede de nada');
		if (socket!=undefined && socket!=null && socket!="") {
		socket.disconnect();		
		}
		
	}else{
		if (socket.socket.connected!=undefined && socket.socket.connected===false) {
		var xhr = Titanium.Network.createHTTPClient();
		xhr.open('GET', Alloy.Globals.server.app+'/Presentacion/OnlinePresentacion.php');
		xhr.onload = function() {
			var json = JSON.parse(this.responseText);
			///SI EL CONDUCTOR ESTA INSCRITO EN EL SISTEMA REMOTO
			if (json.resultado == "true"){
				Ti.API.log('Se va a reconectar');
				
				if (socket==undefined || socket==null || socket=="") {
					socket = io.connect(Alloy.Globals.server.socket, {
						'force new connection' : true
					});
					clearInterval(offline);
					
				} else {
					socket.socket.connect();
				}

			} else {
			}

		};
		xhr.onerror = function(e) {
			
			Ti.App.fireEvent("servicio:estado", {
				"texto" : "Desconectado"
			});
		};
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		var datos = {
			modulo : "iamonline"
		};
		xhr.send(datos);
	}
	}
	
	
}
///////NUEVA VERSION////////////////////////////////
var acciones={
	getPosicionDatos:function(callback){
		    //Ti.API.info("////////Empezando getPosicionDatos");
		   Ti.Geolocation.getCurrentPosition(function(e) {
				var ciudad = "";
				if(!isNaN(parseFloat(e.coords.latitude)) && isFinite(e.coords.latitude) && !isNaN(parseFloat(e.coords.longitude)) && isFinite(e.coords.longitude)){
				var datoscond = CondHandler.getConductor();
				reverseApi(function(resp){
							if(resp.respuesta=="OK"){
								Ti.App.fireEvent("ciudad:origen",{origen:resp.origen.replace(" ", "")});
								callback({
							documento : datoscond.documento,
							imei : datoscond.imei,
							latitud : e.coords.latitude,
							longitud : e.coords.longitude,
							room : "",
							placa : datoscond.placaact,
							paquetes: datoscond.paquetes,
							ciudad : resp.origen.replace(" ", ""),
							estado : datoscond.estado
						});
							}
						},e.coords.latitude, e.coords.longitude);
				
				
				}
		       
		    	});
		 		
	}
};
function reverseApi(callback,latitude,longitude){
			var xhr=Titanium.Network.createHTTPClient();    
    		xhr.open('GET','http://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude); 	
    		xhr.onload = function(e){
    			
                ////Ti.API.info("La respuesta con lat="+latitude+" y long="+longitude+"de google es = "+JSON.stringify(data)); 
    			 if (this.status == 200){	
    			 	var data = JSON.parse(this.responseText);
    			 	////Ti.API.info("La direccion con la que trabajo es  = "+JSON.stringify(data.results[0].formatted_address));
    			 	var dir_array = data.results[0].formatted_address.split(","); 	
    			 	var posbog = dir_array[dir_array.length - 2].toUpperCase();
    			 	
    			 	var ciudad="";
    			 	var departamentos=["CUNDINAMARCA","VALLE DEL CAUCA","META","ANTIOQUIA","ATLÁNTICO","BOLÍVAR"];
						var a = departamentos.indexOf(posbog.replace(" ", ""));
						if(a>=0){
							ciudad = dir_array[dir_array.length - 3].toUpperCase();
							////Ti.API.info("La ciudad es   = "+ciudad);
						}else{
							ciudad=posbog;
							////Ti.API.info("La ciudad es   = "+ciudad);
						}	
					callback({respuesta:"OK",origen:ciudad}); 
                } 
	     		
	                 
   		};
    	xhr.onerror = function(e){
    		//alert('Transmission error: ' + e.error);
    		};
    	xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    	xhr.setRequestHeader("Accept","application/json");
    	xhr.send();  
}


//////////////////////////////////////////////
exports.conductorpublic = conductor;
exports.serviciopublic = servicio;

