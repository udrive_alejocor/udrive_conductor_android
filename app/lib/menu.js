var header;
var	menuLauncher;
var	iconHeader;
var	titleHeader;
var	menuItems;
exports.setWindow = function (window,titulo) {
var	actwin=window;
var menuwin=window;
var osname = Ti.Platform.osname;
var	firstColor='#002039';
var	secondColor='#00375c';
var	textColor='#FFFFFF';
var header = Ti.UI.createView({
	    	backgroundColor: firstColor,
	    	color: textColor,
			height: 50,
			left: 0,
			top: 0,
			width: 'auto',
			zIndex: 10
	});

menuLauncher = Ti.UI.createView({
			backgroundColor: firstColor,
			height: 50,
			left: 0,
			top: 0,
			width: 160
		});
iconHeader = Ti.UI.createButton({
			backgroundImage: '/images/menu.png',
			left: 10,
			width: 30
		});
titleHeader = Ti.UI.createLabel({
			color: textColor,
			font: {
				fontSize: Ti.UI.SIZE,
				fontFamily: 'Roboto-Light'
			},
			left: '50',
			text: titulo,
			textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
		});

menuItems = [
            ['Balance', '', 'Balance','ic_account_balance_wallet_white_24dp.png'],
            ['Regulación', '','Regulación','ic_school_white_24dp.png'],
			['Cerrar', '', 'Cerrar Cuenta','logout.png']		
		];
var menuRows = [];
var menuView = Ti.UI.createView({
			height: 'auto',
			left: -200,
			top: 50,
			width: 200,
			zIndex: 9,
			_toggle: false
	});
var menuTable = Ti.UI.createTableView({
			//backgroundColor: "#eae7e7",
			backgroundColor: "#001020",
			height: 'auto',
			separatorColor: 'transparent',
			width: 'auto'
	});
var menuRows = [];
		
	
		
	//Adding menu options
	for(x in menuItems){
		var row = Ti.UI.createTableViewRow({
				//backgroundColor: "eae7e7",
				//backgroundColor: "#001020",
				className: 'menuRow',
				height: 48,
				objName: 'menuRow',
				selectedBackgroundColor: "#60eae7e7"
				
			}),
			icono=Ti.UI.createImageView({
			 image:'/images/'+menuItems[x][3],
			left: 5,
			width: 24
			}),
			rowLabel = Ti.UI.createLabel({
				//backgroundColor: "#eae7e7",
				backgroundColor: "#001020",
				color: "#FFF",
				font : {
					fontFamily: 'Roboto-Light',
					fontSize: Ti.UI.SIZE
				},
				height: 47,
				text: menuItems[x][0],
				textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
				left:40,
				width: 200,
				_titleView: menuItems[x][2],
				_viewToShow: menuItems[x][1]
			}),
			borderBottom = Ti.UI.createView({
				backgroundColor: "#001020",
				bottom: 0,
				height: 1,
				width: 200
			});
		row.add(icono);
		row.add(rowLabel);
		row.add(borderBottom);
		row.addEventListener('click',menuItemFunction);
		menuRows.push(row);
	}
	menuTable.setData(menuRows);



	//ADDS ZONE
	//iOS hack to center
	if(osname != 'android'){
		iconHeader.top = '40%';
		titleHeader.top = '45%';
	}
	
	menuView.add(menuTable);
	
	actwin.addEventListener('swipe',function(e){
		clickMenu(e.direction);
	});
	menuLauncher.addEventListener('click',function(event){
		clickMenu(null);
	});
	//Adding elements to header
	menuLauncher.add(iconHeader);
	menuLauncher.add(titleHeader);
	header.add(menuLauncher);
	//Adding elements to menu
	menuView.add(menuTable);
	//Adding elements to window
	menuwin.add(header);
	menuwin.add(menuView);

function clickMenu(direction){
		menuLauncher.animate({
		    backgroundColor: secondColor,
		    duration : 200,
		    autoreverse : true,
	    });
		//Menu is hidden
		if(menuView._toggle === false && (direction==null || direction=='right')){
			iconHeader.animate({
				left: -15,
				duration: 200
			});
			menuView.animate({
				left: 0,
				duration: 200,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
			});
			menuView._toggle=true;
		}else if(direction==null || direction=='left'){
			menuView.animate({
				left: -200,
				duration: 200,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
			});
			iconHeader.animate({
				left: 10,
				duration: 200
			});
			menuView._toggle=false;
		};
		
	};
	
	//Function to give functionality to menu items
function menuItemFunction (e){
	//Ti.API.info("informacion del menu= "+JSON.stringify(e)); 
		//Animate background to menu item
		e.source.animate({
		    backgroundColor: secondColor,
		    duration : 200,
		    autoreverse : true,
	    });
		//Hide menuView
		menuView.animate({
			left: -200,
			duration: 200,
			curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		});
		//Move menu icon
		iconHeader.animate({
			left: 10,
			duration: 200
		});
		//Change title to window and deactivate menu flag
		//titleHeader.text=e.source._titleView;
		menuView._toggle=false;
		switch(e.source.text){
			case 'Cerrar':
				 var dialog = Ti.UI.createAlertDialog({
										    cancel: 1,
										    buttonNames: ['Confirmar', 'Cancelar'],
										    message: 'Desea salir y cancelar el servicio?',
										    title: 'Salir'
										  });
										  dialog.addEventListener('click', function(e){
										  	
										    if (e.index != 0){
										      	dialog.hide();
										    }else{
										    	//win.cancelarServicio();
										    	//Ti.API.info(JSON.stringify(e)); 
											  	//Ti.API.info(JSON.stringify(e.source)); 
											  	//Ti.API.info(JSON.stringify(e.index));
        									   	Ti.App.fireEvent("app:logout");
				           						
										    }
										  });
										  dialog.show();
			break;
			case 'FUEC Digital':
					if(Alloy.Globals.FuecDigital){
						
					Alloy.Globals.FuecDigital.open();
					}else{
						var win=Alloy.createController('fuecdigital').getView();
	    			Alloy.Globals.FuecDigital = win;
	 				win.open();
	 				actwin.close();
	 				 // Navigation
			   
					}
				 	
			break;
			case 'Balance':
					if(Alloy.Globals.Balance){
						
					Alloy.Globals.Balance.open();
					}else{
						var win=Alloy.createController('balance').getView();
	    			Alloy.Globals.Balance = win;
	 				win.open();
	 				 // Navigation
			   
					}
				 	
			break;
		}
	};
};
exports.setTitulo=function(titulo){
	titleHeader.text=titulo;
};
