var Paquetes;
var Tarifas;
var PaqueteManual="";
var OrigenDestino={}; 
if(Ti.App.Properties.hasProperty('Paquetes')){
		var Paquetes=Ti.App.Properties.getList('Paquetes');
	}
if(Ti.App.Properties.hasProperty('PaqueteManual')){
		var PaqueteManual=Ti.App.Properties.getString('PaqueteManual');
	}
if(Ti.App.Properties.hasProperty('Tarifas')){
		var Tarifas=Ti.App.Properties.getList('Tarifas');
	}
if(Ti.App.Properties.hasProperty('OrigenDestinos')){
		var OrigenDestino=Ti.App.Properties.getList('OrigenDestinos');
	}
exports.setOrigenDestino = function(datos) {
	Ti.App.Properties.setList('OrigenDestinos',datos);
};	
exports.setPaquetes = function(datos) {
	Paquetes=datos;
	Ti.App.Properties.setList('Paquetes',Paquetes);
	////Ti.API.info("El paquete configurado es = "+JSON.stringify(datos));
};	
exports.getPaquete = function(id) {
	//Paquetes[id]=datos;
	return Paquetes[id];
};
exports.getPaqueteManual = function() {
	//Paquetes[id]=datos;
	return PaqueteManual;
};
exports.getDestinos = function(key) {
	////Ti.API.info("origen destino actual = "+JSON.stringify(OrigenDestino)+" y key="+key);
	OrigenDestino=Ti.App.Properties.getList('OrigenDestinos');
	////Ti.API.info("Las origenes destinos configurados son = "+JSON.stringify(OrigenDestino[key]));
	return OrigenDestino[key];
};
exports.setTarifas = function(datos) {
	Tarifas=datos;
	Ti.App.Properties.setList('Tarifas',Tarifas);
	////Ti.API.info("Las tarifas configuradas son = "+JSON.stringify(datos));
};	
exports.getTarifa = function(paquete,servicio,origen,destino){
	return Tarifas[""+paquete+":"+servicio+":"+origen+":"+destino+""];
};
exports.salir = function(){
var Paquetes;
var Tarifas;
var PaqueteManual="";
var OrigenDestino={}; 
Tarifas=Ti.App.Properties.getList('Tarifas');
Paquetes=Ti.App.Properties.getList('Paquetes');
PaqueteManual=Ti.App.Properties.getString('PaqueteManual');
OrigenDestino=Ti.App.Properties.getList('OrigenDestinos');
};
exports.updatePaquetesPlaca = function(username,password,placa,origen, callback) {
		//Ti.API.info("Si entra  = "+Alloy.Globals.server.api+'v1/conductorespaquetes?placa='+placa+'&ciudad='+origen);
       		var xhr=Titanium.Network.createHTTPClient();    
       		//Ti.API.info("La informacion para obtener paquetes placas es  = "+Alloy.Globals.server.api+'v1/conductorespaquetes?placa='+placa+'&ciudad='+origen);  
    		xhr.open('GET',Alloy.Globals.server.api+'v1/conductorespaquetes?placa='+placa+'&ciudad='+origen); 	
    		xhr.onload = function(e){
    			var data = JSON.parse(this.responseText);
                ////Ti.API.info("La respuesta de intelligent es = "+JSON.stringify(data));  
    			 if (this.status == 200) {
					var paqconductor=[];
					var paquetes={};
					var tarifas={};
					var ordest={};
    			 	for(var i=0;i<data.paquetes.length;++i){
    			 		paqconductor.push(data.paquetes[i].paquetes_id);
								        paquetes[""+data.paquetes[i].paquetes_id+""]={
								       	paquete:data.paquetes[i].paquete,
								       	empresa:data.paquetes[i].contratante,
								        descuento:data.paquetes[i].contratante,
										nit:data.paquetes[i].nit,
										logo:"",
										header:"",
										background:"",
										fuec:data.paquetes[i].fuec,
										muestratarifa:data.paquetes[i].muestratarifa,
										fuecurl:data.paquetes[i].archivo,

								    };
					//paquete para hacer fuecs manuales
					if(data.paquetes[i].fuec=="MANUAL"){
						PaqueteManual=data.paquetes[i].paquetes_id;
						Ti.App.Properties.setString('PaqueteManual',PaqueteManual);
					}
					}
					var array=[];
					for(var i=0;i<data.tarifas.length;++i){
					 	if(array.indexOf(""+data.tarifas[i].paquetes_id+":"+data.tarifas[i].origen+"")==-1){
					 		array.push(""+data.tarifas[i].paquetes_id+":"+data.tarifas[i].origen+"");
					 		OrigenDestino[""+data.tarifas[i].paquetes_id+":"+data.tarifas[i].origen+""]=[data.tarifas[i].destino];
					 	}else{
					 		OrigenDestino[""+data.tarifas[i].paquetes_id+":"+data.tarifas[i].origen+""].push(data.tarifas[i].destino);
					 	}
					 	tarifas[""+data.tarifas[i].paquetes_id+":"+data.tarifas[i].servicio_id+":"+data.tarifas[i].origen+":"+data.tarifas[i].destino]={
					 	tarifaid:data.tarifas[i].id,
					 	paquete:data.tarifas[i].paquete,
					 	origen:data.tarifas[i].origen,
						destino:data.tarifas[i].destino,
						unidadseg:data.tarifas[i].unidadseg,
						tarifaundseg:data.tarifas[i].tarifaundseg,
						unidadkm:data.tarifas[i].unidadkm,
						tarifaundkm:data.tarifas[i].tarifaundkm,
						minima:data.tarifas[i].minima,	
						comienzaen:data.tarifas[i].comienzaen,
					 };
						
					}
					exports.setOrigenDestino(OrigenDestino);
					exports.setPaquetes(paquetes);
					exports.setTarifas(tarifas);
					callback({ result: 'ok',paquetes:paqconductor });
                } else {
                	callback({ result: 'error', msg: "Esta placa no tiene paquetes activos" });
                }
	     		
	                 
   		};
    	xhr.onerror = function(e){
    		//alert('Transmission error: ' + e.error);
    		};
    	xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    	xhr.setRequestHeader("Accept","application/json");
    /*	var array={
    					documento:username,
    					ciudad:origen,
    					placa:placa
    				};
    	xhr.send(JSON.stringify(array)); */ 
    	xhr.send();                 
};


