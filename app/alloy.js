// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
if( OS_ANDROID ) {
    Alloy.Globals.Android = { 
        'Api' : Ti.Platform.Android.API_LEVEL
    };
}
// Styles
Alloy.Globals.Styles = {
    'TableViewRow' :{
        'height' : 45
    }
};
Alloy.Globals.sonido = Titanium.Media.createSound({
				url: '/sound/nuevoservicio.mp3',
				preload: true
			});

Alloy.Globals.chat = Titanium.Media.createSound({
				url: '/sound/chat.mp3',
				preload: true
			});
Alloy.Globals.server = {
				//api: 'http://192.168.1.6:80/aws_udriverapi/udriverapi/web/app_dev.php/app/api/',
				api: 'http://udriverapi.ibusinesscolombia.com/app_dev.php/app/api/',//prod
				//api: 'http://udriverapiqas.ibusinesscolombia.com/app_dev.php/app/api/',//qas
				app:'http://udriverapp.ibusinesscolombia.com',//produccion
				//app:'http://udriverappqas.ibusinesscolombia.com',//qas
				socket: 'http://udriver-frontend-183682246.us-east-1.elb.amazonaws.com:82',//prod
				//socket: 'http://udriversocketsqas.ibusinesscolombia.com:82',//qas
				//imagenes:'http://192.168.1.6:80/',
			};
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
};
