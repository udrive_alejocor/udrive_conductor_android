// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var AppData = require('ConductorHandler');
var BalanceData = require('BalanceplacaHandler');
var balance={
	updateBalance:function(){
		var conductor=AppData.getConductor();
		var token=AppData.getTokenConductor();
		var xhr=Titanium.Network.createHTTPClient();    
		//Ti.API.info("la url balance  = "+Alloy.Globals.server.api+'v1/balancevehiculos?token='+token+'&usuario='+conductor.documento+'&placa='+conductor.placaact);
		xhr.open('GET',Alloy.Globals.server.api+'v1/balancevehiculos?token='+token+'&usuario='+conductor.documento+'&placa='+conductor.placaact); 	
    		xhr.onload = function(e){
    			var data = JSON.parse(this.responseText);
    			
                   Ti.API.info("La respuesta de intelligent es = "+JSON.stringify(data));
                 
    			 if (data.resultado == 0) {
    			    balance.showBalance(data.datos[0]);
                } else {
                	alert("Parece que tienes problemas con la conexión");
                	balance.showBalance(BalanceData.getBalance());
                }
	     		
	                 
   		};
    	xhr.onerror = function(e){
    		
    		};
    	xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    	xhr.setRequestHeader("Accept","application/json"); 
    	xhr.send();                
	},
	showBalance:function(datos){
		BalanceData.setBalance(datos);
		var saldo=((parseFloat(datos.tcredito)*0.8)+parseFloat(datos.recibido))-((parseFloat(datos.efectivo)*0.2)+parseFloat(datos.pagado));
		//Ti.API.info("El saldo es  = "+saldo);
		$.totalval.setShadowColor("#e2f3ed");
		
		$.totalval.setText(balance.currencyFormat(saldo));
		$.canttc.setText("Total "+datos.stcredito+" servicios");
		$.tottc.setText(balance.currencyFormat(parseFloat(datos.tcredito)));
		$.cantefec.setText("Total "+datos.sefectivo+" servicios");
		$.totefec.setText(balance.currencyFormat(parseFloat(datos.efectivo)));
		if(saldo>=0){	
			$.totalval.setColor("#a1f4a3");	
			$.destotal.setText("El saldo del vehículo con placas "+datos.placa+" es positivo y se le consignará en el próximo corte");
		}
		else{
			$.totalval.setColor("#de2c7b");
			$.destotal.setText("El saldo del vehículo con placas "+datos.placa+" es negativo, debe consignar antes del próximo corte, recuerde que de no hacerlo el sistema suspendera la cuenta del vehículo");
		}
		
	},
	currencyFormat:function(num) {
    return "$" + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
	}
};
function closeBalance(){
	$.Balance.close();
	Alloy.Globals.Balance=null;
}

$.cleanup = function cleanup() {
  $.destroy();
  $.off();
  	$.Balance.removeEventListener('close', $.cleanup);
	$.Balance.removeEventListener('open', balance.updateBalance);
    
};
$.Balance.addEventListener('open', balance.updateBalance);
$.Balance.addEventListener('close', $.cleanup);
