var AppData = require('ConductorHandler');
if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {
   		alert(' No estas conectado a internet, por favor conectate e intentalo de nuevo ');
	} 
	else{
   		if(AppData.getLoggedIn()==false){
    		var loginController = Alloy.createController('login');
			} 
			else{
					//Ti.UI.iPhone.appBadge = 0;
 					var win=Alloy.createController('principal').getView();
	    			Alloy.Globals.Principal = win;
	 				win.open();
	 				 // Navigation
			    //
		     	// Android
		     	
			    if (OS_ANDROID) {
			    	
			        Alloy.Globals.Principal.addEventListener('open', function() {
			        	//Ti.App.fireEvent("principal:activarWS");
			            if(Alloy.Globals.Principal.activity) {
			                var activity = Alloy.Globals.Principal.activity;
			    
			                // Action Bar
			                if( Ti.Platform.Android.API_LEVEL >= 11 && activity.actionBar) {      
			                  //  activity.actionBar.title = L('appTitle', 'Udriver'); 
			                    activity.actionBar.hide();           
			                }
			            }   
			        });
			        
				        // Back Button
				        Alloy.Globals.Principal.addEventListener('android:back', function() {
				           if(AppData.getConductor().estado!="LIBRE"){
				           var dialog = Ti.UI.createAlertDialog({
										    cancel: 1,
										    buttonNames: ['Confirmar', 'Cancelar'],
										    message: 'Desea salir y cancelar el servicio?',
										    title: 'Salir'
										  });
										  dialog.addEventListener('click', function(e){
										    if (e.index === 1){
										      	dialog.hide();
										    }else{
										    	 Ti.App.fireEvent("conductor:cancela");
										    	var activity = Ti.Android.currentActivity;
				           						 activity.finish();
										    }
										  });
										  dialog.show();
										  }
				        });
			    }   
	 				
     
   		};
    	
  }
