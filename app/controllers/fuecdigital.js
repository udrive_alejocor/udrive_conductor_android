var args = arguments[0] || {};
var AppData = require('ConductorHandler');
var AppPaquete = require('PaqueteHandler');
var registro={
	paquete:"",
	fuec:"",
	aceptaservicio:false,
	tdocumentos:['CC','NIT','CE','Pasaporte'],
	registrar:function(){
		if(registro.paquete!=""){
		registro.validar(function(respuesta){
			 Ti.API.info("La respuesta de la validacion es = "+JSON.stringify(respuesta)); 
			if(respuesta.resp==1){
			$.buttonCrear.enabled = false;
			var xhr=Titanium.Network.createHTTPClient();    
    		xhr.open('GET',Alloy.Globals.server.app+'/Presentacion/ContratoPresentacion.php'); 	
    		xhr.onload = function(e){
    			var data = JSON.parse(this.responseText);
                Ti.API.info("La respuesta de intelligent elaborando el contrato = "+JSON.stringify(data));  
                if(data.resultado=="true"){
                	
                	registro.limpiarDatos();
                	registro.fuec=data.archivo;
                	$.fuecdigital.visible=true;
                	$.buttonCrear.enabled = true;
                }else{
                	alert(datos.mensaje);
                	$.buttonCrear.enabled = true;
                }		
	                 
   		};
    	xhr.onerror = function(e){
    		//alert('Transmission error: ' + e.error);
    		};
    	xhr.setRequestHeader("Content-Type","application/json ");
    	xhr.setRequestHeader("Accept","application/json");
    	var array={
						pasajero_nom:$.inputNombre.value,
						pasajero_ape:$.inputApellido.value,
						tdocumento:$.inputTdocumento.value,
						pasajeros_doc:$.inputDocumento.value,
						correo:$.inputCorreo.value,
						celular:$.inputCelular.value,
						direccion:$.inputDireccion.value,
						origen: $.inputOrigen.value,
						destino:$.inputDestino.value,
						fechaini: $.inputFechaini.value,
						fechafin:$.inputFechafin.value,
						recorrido:$.inputRecorrido.value,
						vehiculo: AppData.getConductor().vehiculoact.id,
						conductor: AppData.getConductor().id,
						paquete:registro.paquete
    				};
    				 //Ti.API.info("la info para elaborar el contrato es  = "+JSON.stringify(array));
    	var datos={
    				modulo:"getContratoManual",
    				datos:JSON.stringify(array),
    				};
    				 Ti.API.info("la info para elaborar el contrato es  = "+JSON.stringify(datos)); 

    	xhr.send(datos); 
    		
		}else{
			alert(respuesta.mensaje);
		}
		});
		
					    
			
	}else{
		alert("No tiene el servicio de fuec digital activo, por favor comuniquese con su empresa para habilitar el servicio");
	}			
	},
	validar:function(callback){
		var resp=1;
		var mens="";
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		if($.inputNombre.value==""){
			resp=0;
			mens +="Falra ingresar un nombre. ";
		}
		if($.inputApellido.value==""){
			resp=0;
			mens +="Falta ingresar un apellido. ";
		}
		if(!re.test($.inputCorreo.value)){
	
			mens +="El correo del contratante no es válido. ";
			resp=0;
		}
		if(!re.test($.inputCorreo2.value)){
	
			mens +="Ingresa un correo válido para poder enviar la copia del Contrato. ";
			resp=0;
		}
		if($.inputCelular.value==""){
			mens +="Falta ingresar el correo . ";
			resp=0;
		}
		if($.inputDireccion.value==""){
			mens +="Falta ingresar la dirección . ";
			resp=0;
		}
		if($.inputFechaini.value==""){
			mens +="Falta ingresar la fecha de inicio del contrato . ";
			resp=0;
		}
		if($.inputFechafin.value==""){
			mens +="Falta ingresar la fecha final del contrato . ";
			resp=0;
		}
		if($.inputOrigen.value==""){
			mens +="Falta ingresar la ciudad de Origen. ";
			resp=0;
		}
		if($.inputDestino.value==""){
			mens +="Falta ingresar la ciudad de Destino. ";
			resp=0;
		}
		
		callback({"resp":resp,"mensaje":mens});
	},
	showtdoclist:function(){
		 var opts = {
		  cancel: 2,
		  options: registro.tdocumentos,
		  selectedIndex: 2,
		  destructive: 0,
		};
		dialog = Ti.UI.createOptionDialog(opts);
	    dialog.show();
	    dialog.addEventListener('click', registro.onSelectDialog);
	},
	onSelectDialog:function(event){
		 var selectedIndex = event.source.selectedIndex;
		 $.inputTdocumento.value=registro.tdocumentos[selectedIndex];
	},
	validaServicio:function(){
		if(AppPaquete.getPaqueteManual()==""){
			alert("No tiene el servicio de fuec digital activo, por favor comuniquese con su empresa para habilitar el servicio");
		}else{
			Ti.API.info("El paquete manual es el  = "+AppPaquete.getPaqueteManual());
			registro.paquete=AppPaquete.getPaqueteManual();
		}
	},
	Verfuec:function(){
		if(registro.fuec!=""){
			Ti.Platform.openURL(Alloy.Globals.server.app+'/contratos/'+registro.fuec);
		}
		
	},
	closeFuec:function(){
		$.fuecdigital.visible=false;
	},
	limpiarDatos:function(){
		$.inputNombre.setValue("");
		$.inputApellido.setValue("");
		$.inputCorreo.setValue("");
		$.inputCelular.setValue("");
		$.inputDireccion.setValue("");
		$.inputFechaini.setValue("");
		$.inputFechafin.setValue("");
		$.inputOrigen.setValue("");
		$.inputDestino.setValue("");
		$.inputRecorrido.setValue("");
	}
};


 //
		     	// Android
		     	
			    if (OS_ANDROID) {
			    	
			        $.FuecDigital.addEventListener('open', function() {
			        	//Ti.App.fireEvent("principal:activarWS");
			            if($.FuecDigital.activity) {
			                var activity = $.FuecDigital.activity;
			    
			                // Action Bar
			                if( Ti.Platform.Android.API_LEVEL >= 11 && activity.actionBar) {      
			                  //  activity.actionBar.title = L('appTitle', 'Udriver'); 
			                    activity.actionBar.hide();           
			                }
			            }   
			        });
			        
				        // Back Button
				    $.FuecDigital.addEventListener('android:back', function() {
															   	Alloy.Globals.Principal.open();
																$.FuecDigital.close();
																Alloy.Globals.FuecDigital=null;				        });
			    	}  

$.cleanup = function cleanup() {
  $.destroy();
  $.off();
  $.FuecDigital.removeEventListener('close', $.cleanup);
  $.FuecDigital.removeEventListener('open', registro.validaServicio);

};
$.FuecDigital.addEventListener('close', $.cleanup);
$.FuecDigital.addEventListener('open', registro.validaServicio);

 