var args = arguments[0] || {};
var ServHandler = require('ServicioHandler');
var AppData = require('ConductorHandler');
var AppPaquete = require('PaqueteHandler');
var AppGeo = require('LocalizacionHandler');
var waze = require('waze');		
var SincronizarFuec;
var tomarTempo;  
var tomado=false;   
var tomar=0;
var menu = require('menu');
var myVer = Ti.App.version;
Ti.API.info("LA VERSION DE MI APLIACION ="+myVer);
if (!AppData.getLoggedIn()){
    var loginController = Alloy.createController('login');
}
else{
	
	
	menu.setWindow($.Principal,'Udrive');
	Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;
	Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
	Ti.Geolocation.distanceFilter = 10;
	Ti.Geolocation.purpose = "Recibir la localizacion";
	var sockws=null;
	var udimetro;
	var principal={
	placas:[],
	mensaje:"",
	configWindow:function(){
			var imageView = Titanium.UI.createImageView({
				id:'imgdriver',
			    image:'http://uappconductor.s3.amazonaws.com/imgconductores/'+AppData.getConductor().documento+'.jpg',
			    //image:'http://uappconductor.s3.amazonaws.com/imgconductores/79087645.jpg',
			    height: 100,
			    width: 100,
			    borderRadius:50,
			    shadowColor: '#EDDD07',
			    shadowOffset: {x:5, y:5},
			    shadowRadius: 2,
			    marginleft: 10
			});
		
		$.imagendriver.add(imageView);
		$.nombre.setText(AppData.getConductor().nombre);
		//$.empresa.setText(AppData.getConductor().nombre);
		$.placa.setText(AppData.getConductor().placaact);
		$.idmovil.setText(AppData.getConductor().documento);
	},
	showConexion:function(datos){
		$.estado.setText(datos.texto);
		$.estadoview.setBackgroundColor(datos.color);
	},
	/*
	 * 1.Verificamos si tiene placa actual asignada
	 *   Si la tiene entonces puede iniciar sesion de socket con el paquete asociado.
	 * 	 Si no, consultamos las placas asociadas a este conductor y una vez seleccionada una de ellas se configura el paquete actual
	 */
	verificarPlaca:function(){
		//Ti.API.info("Entra a verificar placa catual="+AppData.getConductor().placaact);
		if(AppData.getPlaca()=="" || AppData.getConductor().placaact==undefined){
			principal.getPlacas();
		}else{
			principal.activarSocket();
			switch(AppData.getEstado()){
				case "ENCAMINO":
				var servicio= Ti.App.Properties.getList('pasajerodat');
				$.pasajero.setText(servicio.nombre);
				$.direccion.setText(servicio.direccion);
				$.recaudo.setText(servicio.fpago.nombre);
				//Ti.Geolocation.getCurrentPosition(updatePositionDistancia);
		        //Ti.Geolocation.addEventListener('location', updatePositionDistancia);
		        $.botonesNuevoServ.visible = false;
				$.NuevoSer.visible=true;
			    $.bottomBar.visible=true;
			   startposVehiculo();
				break;
				case "ENCARRERA":
				  if(ServHandler.getPaquete()!=""){
				  $.servorigen.getValue(ServHandler.getTarifa().origen);
				  $.servdestino.setValue(ServHandler.getTarifa().destino);
				  $.recaudoText.setText(ServHandler.getServiciofpago().nombre);
				  if(AppPaquete.getPaquete(ServHandler.getPaquete()).fuec=="AUTOMATICO"){
				  	if(ServHandler.getArchivo()==""){
				  		ServHandler.mostrarFuec();
					}else{
						$.verfuec.addEventListener('click', principal.showFuec);
						$.verfuec.setBackgroundColor("#00375c");
				  	}
				     
						//Ti.API.info("El fuec que se va a mostrar es   = el archivo "+AppPaquete.getPaquete(ServHandler.getPaquete()).fuecurl);
					}
					var popup_animation = Titanium.UI.createAnimation();		
						popup_animation.left = 0;
						popup_animation.right = 0;
						popup_animation.duration = 500;
						$.bottomBar2.animate(popup_animation);
						$.bottomBar2.setWidth(Ti.UI.FILL);
					//empezar el udimetro
					udimetro = require('udimetro2');
					udimetro.retomar();
					//enviar aviso a la app del pasajero para que quite el cancelar
					}else{
						sockws.activarLibre();
					}
				break;
			}
		}
	},
	getPlacas:function(){
        //consultamos placas asociadas a conductor	
        var placa=AppData.getPlacas();
		principal.placas=placa;
		var optionsDialog = Ti.UI.createOptionDialog({
			      selectedIndex: 0,
				  title: "Seleccione el vehiculo actual",
				  options: placa
				});
			optionsDialog.addEventListener('click',principal.setPaquetes);
			
			optionsDialog.show();
	},
	setPaquetes:function(e){
		
		var opt=(e.index<0) ? 0 : e.index;
		//configuramos placa seleccionada
		Ti.API.info("las placas configuradas en index="+opt+" son  = "+JSON.stringify(principal.placas[opt])); 
		AppData.setPlaca(principal.placas[opt]);
		$.placa.setText(AppData.getConductor().placaact);
		 
		//configuramos paquetes asociados a la placa con la ciudad actual del vehiculo
		AppGeo.getOrigen(function(datos){
			AppPaquete.updatePaquetesPlaca("username","password",AppData.getConductor().placaact,datos.ciudad,
	    function(response) {
			 if(response.result === "ok") {
			    AppData.setPaquetes(response.paquetes);
			    if(sockws==null){
			    principal.activarSocket(); 
			    }else{
			    principal.cambiarSocket();
			    }          
			 }else{
			    alert(L('error', 'Error') + ':\n' + response.msg);
			      }
		});
		});
	    		
	},
	updatePaquetes:function(){
		//configuramos placa seleccionada
		Ti.API.info("actualizacion de paquetes segun la placa actual"); 
		//configuramos paquetes asociados a la placa con la ciudad actual del vehiculo
		AppGeo.getOrigen(function(datos){
			Ti.API.info("los datos de appgeo son  = "+JSON.stringify(datos));
			AppPaquete.updatePaquetesPlaca("username","password",AppData.getConductor().placaact,datos.ciudad,
	    function(response) {
			 if(response.result === "ok"){
			    AppData.setPaquetes(response.paquetes);
			    if(sockws==null){
			    principal.activarSocket(); 
			    }else{
			    principal.cambiarSocket();
			    }          
			 }else{
			    alert(L('error', 'Error') + ':\n' + response.msg);
			      }
		});
		});
	    		
	},
	cambiarSocket:function(){
		
	},
	testGPS: function(_callback) {
    if (_callback && typeof(_callback)==='function') {
        Ti.Geolocation.getCurrentPosition(function(e) {
                _callback ((e.error) ? false: true);
        }); 
    }
	},
	activarSocket:function(){
		//Ti.API.info("activando socket y geolocation es "+Ti.Geolocation.locationServicesEnabled);
		 if (Ti.Geolocation.locationServicesEnabled === false || Ti.Geolocation.locationServicesEnabled === "false" || Ti.Geolocation.locationServicesEnabled === undefined || Ti.Geolocation.locationServicesEnabled === "undefined") {
		 	alert('El GPS se encuentra apagado . Es necesario encenderlo y volver a ingresar.');
		 }else{
		 	   
				Ti.App.addEventListener("app:got.location", function(d) {
				sockws = require('conectaws');
			    sockws.setwin($.Principal);
			    sockws.setdir($.direccion);
			    sockws.setrecaudo($.recaudo);
			    sockws.setpasajero($.pasajero);
			    sockws.setdistanciapas($.distaciapas);
			    sockws.setventana($.NuevoSer);
		        Ti.Geolocation.removeEventListener('location', updatePosition);
		    	});
		 		Ti.Geolocation.getCurrentPosition(updatePosition);
		    	Ti.Geolocation.addEventListener('location', updatePosition);	
		 }
			
			
		
	},
	setOrigen:function(data){
		$.servorigen.setValue(data.origen);
		principal.updatePaquetes();
	},
	setDestino:function(data){
		$.servdestino.setValue(data.origen);
	},
	tomarServicio:function(){
     AppData.setEstado("ENCAMINO");
		$.botonesNuevoServ.visible = false;
		sockws.tomarServicio();//envio deseo de tamor servicio
		//$.NuevoSer.animate({top:520,duration:500});
		
		/*Ti.Geolocation.getCurrentPosition(updatePositionDistancia);
		Ti.Geolocation.addEventListener('location', updatePositionDistancia);*/
		startposVehiculo();
	},
	Tomado:function(){
		$.bottomBar.visible=true;
		$.tomar.enabled = true;
		tomado=true;
		
		$.distaciapas.addEventListener('click', principal.openWaze);
		//startposVehiculo();
	},
	openWaze:function(){
		var servicio= Ti.App.Properties.getList('pasajerodat');
		waze.NavigateTo(servicio.latitud,servicio.longitud);
	},
	descartar:function(){
		Alloy.Globals.sonido.stop();
		//$.NuevoSer.animate({top:520,duration:500});
		$.botonesNuevoServ.visible = true;
		$.NuevoSer.visible=false;
		tomado=false;
		sockws.desbloquear();
		 	
	},
	clienteCancela:function(){
		stopposVehiculo();
		$.chat.visible=false;
		principal.limpiarChat();
		$.bottomBar.visible=false;
		$.botonesNuevoServ.visible = true;
		$.NuevoSer.visible=false;
		tomado=false;
		Ti.Geolocation.removeEventListener('location', updatePositionDistancia);
        $.distaciapas.removeEventListener('click', principal.openWaze);
	},
	servicioRechazado:function(e){
		principal.limpiarChat();
		tomado=false;
		$.botonesNuevoServ.visible = true;
		$.NuevoSer.visible=false;
		$.bottomBar.visible=false;
		stopposVehiculo();
		Ti.Geolocation.removeEventListener('location', updatePositionDistancia);
			alert(e.resp);
	},
	cancelar:function(){
		var dialog = Ti.UI.createAlertDialog({
						cancel: 1,
						buttonNames: ['Cancelar Servicio', 'Cerrar'],
						message: 'Recuerda que tu cliente esta esperandote, puedes enviarle primero un mensaje por el chat, ¿Deseas cancelar el servicio ?',
						title: 'Cancelar Servicio en Camino'
					});
					dialog.addEventListener('click', function(e){
							if (e.index === 1){
									dialog.hide();
							}else{
								principal.limpiarChat();
								//Ti.API.info("VOY A CANCELAR ");
								sockws.cancelarServicio(ServHandler.getServicioRoom());
								$.distaciapas.removeEventListener('click', principal.openWaze);
										    }
							});
							dialog.show();
		
	},
	concelarOk:function(){
		stopposVehiculo();
		tomado=false;
		$.bottomBar.visible=false;
		$.botonesNuevoServ.visible = true;
		$.NuevoSer.visible=false;
		Ti.Geolocation.removeEventListener('location', updatePositionDistancia);
		principal.limpiarChat();
	},
	usuarioAborda:function(){
		var dialog = Ti.UI.createAlertDialog({
						cancel: 1,
						buttonNames: ['Comenzar Servicio', 'Cerrar'],
						message: 'Si tu pasajero ya esta dentro del vehiculo puedes comenzar el servicio y elegir el destino, ¿Deseas comenzar el servicio ?',
						title: 'Comenzar Servicio'
					});
					dialog.addEventListener('click', function(e){
							if (e.index === 1){
									dialog.hide();
							}else{
								sockws.abordoCliente();
								principal.limpiarChat();
								$.chat.visible=false;							
								Ti.Geolocation.removeEventListener('location', updatePositionDistancia);
								stopposVehiculo();
								$.botonesNuevoServ.visible = true;
								$.NuevoSer.visible=false;
								$.distaciapas.removeEventListener('click', principal.openWaze);
								ServHandler.sincronizarAbordo(1);
								
								//enviar aviso a la app del pasajero para que quite el cancelar
								
								ServHandler.informarAbordo(function(sinc){
									
								});
								
								//ServHandler.setTarifa(AppPaquete.getDestinos(ServHandler.getPaquete+$.servorigen.getValue()));
								//Ti.API.info("Los destinos posibles son "+JSON.stringify(AppPaquete.getDestinos(Ti.App.Properties.getList('pasajerodat').paquete+":"+$.servorigen.getValue())));
								var destinos=AppPaquete.getDestinos(ServHandler.getPaquete()+":"+$.servorigen.getValue());
								var array=[];
								for(var i=0;i<destinos.length;i++){
									array[i]={ sel_dest: {text: destinos[i]}};
								}
							$.destinoslist.setItems(array);
							$.destinosmodal.visible=true;
								//AppPaquete.setTarifas();
							}
							});
							dialog.show();
		
		
	},
	selecDestino:function(e){
		stopposVehiculo();
		var item = $.destinoslist.getItemAt(e.itemIndex);
		ServHandler.setTarifa(AppPaquete.getTarifa(ServHandler.getPaquete(),ServHandler.getServicioid(),$.servorigen.getValue(),item.sel_dest.text));
		ServHandler.setMtarifa(AppPaquete.getPaquete(ServHandler.getPaquete()).muestratarifa,function(resp){
			AppData.setEstado("ENCARRERA");
			$.tarifatext.setValue(ServHandler.getTarifa().comienzaen);
			
			if(resp=="ok"){
				//empezar el udimetro
				udimetro = require('udimetro2');
				udimetro.comenzar();
			}
			
		});
		$.servdestino.setValue(item.sel_dest.text);
		$.recaudoText.setText(ServHandler.getServiciofpago().nombre);
		
		$.destinosmodal.visible=false;
		$.bottomBar.visible=false;
		var popup_animation = Titanium.UI.createAnimation();		
			popup_animation.left = 0;
			popup_animation.right = 0;
			popup_animation.duration = 500;
			$.bottomBar2.animate(popup_animation);
			$.bottomBar2.setWidth(Ti.UI.FILL);
		//descargar pdf de fuec segun sea el caso
		if(AppPaquete.getPaquete(ServHandler.getPaquete()).fuec=="AUTOMATICO"){
			ServHandler.mostrarFuec();
		}else{
			//Ti.API.info("El fuec que se va a mostrar es   = el archivo "+AppPaquete.getPaquete(ServHandler.getPaquete()).fuecurl);
		}
		
		
	},
	setFuec:function(){
		if (AppData.getEstado()=="ENCARRERA") {
			$.verfuec.addEventListener('click', principal.showFuec);
	   		$.verfuec.setBackgroundColor("#00375c");
		};
		
	},
	showFuec:function(){
		if(ServHandler.getArchivo()!=""){
			Ti.Platform.openURL(Alloy.Globals.server.app+'/contratos/'+ServHandler.getArchivo());
		}
		
	},
	terminarServicio:function(){	
		udimetro.detener(function(callback){
			if(callback.resultado=="ok"){			 
				var popup_animation = Titanium.UI.createAnimation();		
					popup_animation.left = -600;
					popup_animation.duration = 500;
					$.bottomBar2.setWidth(Ti.UI.SIZE);
					$.bottomBar2.animate(popup_animation);
			    var muestratarifa=AppPaquete.getPaquete(ServHandler.getPaquete()).muestratarifa;
			    var precio=ServHandler.getPrecio(1);
			    var room=ServHandler.getServicioRoom();
				
				//SI PAQUETE MUESTRA TARIFA ENTONCES muestra TARIFA A CLIENTE
				if(AppPaquete.getPaquete(ServHandler.getPaquete()).muestratarifa=="SI"){
					
///////////////////   LANZAMIENTO CON LA COPA AMERICA, BORRAR PARA POSTERIORES VERSIONES   /////////////////////////////////////////////////
                     var tiquete=ServHandler.getTiquete().toLocaleUpperCase();;
                     var msg="El precio del servicio es $"+precio;
                      if(tiquete=="#UDRIVEESCOLOMBIA"){
                      	var descuento=AppPaquete.getPaquete(ServHandler.getPaquete()).descuento;
                      	var totaldesc=0;
                      	if(descuento>0){
                      		if(descuento==1){
                      			totaldesc=precio-(precio*0.2);
                      			totaldesc=Math.round(totaldesc/50)*50;
                      			$.tarifatext.setValue(totaldesc);
                      			msg="Gooool, te ganaste el 20% de descuento  solo debes pagar $"+totaldesc;
                      			alert("El precio del servicio es $"+totaldesc);
                      		}
                      		if(descuento==2){
                      			totaldesc=precio-(precio*0.3);
                      			totaldesc=Math.round(totaldesc/50)*50;
                      			$.tarifatext.setValue(totaldesc);
                      			msg="Gooool, te ganaste el 30% de descuento  solo debes pagar $"+totaldesc;
                      			alert("El precio del servicio es $"+totaldesc);
                      		}
                      		if(descuento>=3){
                      			totaldesc=precio-(precio*0.5);
                      			totaldesc=Math.round(totaldesc/50)*50;
                      			$.tarifatext.setValue(totaldesc);
                      			msg="Gooool, te ganaste el 50% de descuento  solo debes pagar $"+totaldesc;                			
                      			alert("El precio del servicio es $"+totaldesc);
                      		}
                      		precio=totaldesc;
                      		
                      	}else{
                      		alert(msg);
                      	}    	
                      }else{
                      	alert(msg);
                      }					   
						
						
					}
				if(AppPaquete.getPaquete(ServHandler.getPaquete()).fuec=="AUTOMATICO"){
					$.verfuec.setBackgroundColor("#B2B1B0");
					$.verfuec.removeEventListener('click', principal.showFuec);
				}
				sockws.finCarrera(muestratarifa,msg,room);
				
				
			   //GUARDAR LA CARRERA EN DISPOSITIVO Y EN EL SERVIDOR
				ServHandler.sincronizar(1,function(resp){
					if(resp=="ok"){
						ServHandler.limpiaDatos();
						tomado=false;
						$.tomar.enabled = true;
						$.descartar.enabled = true;
					}
				});
				
				
			}
		});
		$.distaciapas.removeEventListener('click', principal.openWaze);
		$.tarifatext.setValue(0);
	},
	mostrarTarifa:function(datos){
		$.tarifatext.setValue(ServHandler.getPrecio(datos.final));
	},
	closeDestino:function(){
		$.destinosmodal.visible=false;
	},
	openChat:function(){
		$.NuevoSer.visible=false;
		$.chat.visible=true;
	},
	closeChat:function(){
		$.chat.visible=false;
		$.NuevoSer.visible=true;
	},//7954746
	selecMensaje:function(e){
		itemId = e.source;
    	 for ( var x in $.chatview.children ) {
		    if ($.chatview.children[x].children[0].id != itemId.children[0].id) {
		       $.chatview.children[x].children[0].visible=false;
		    }
		  }
    	itemId.children[0].visible=true;
    	//Ti.API.info("el mensaje para enviar es  = " + itemId.children[1].text);
    	$.respuestachat.setValue(itemId.children[1].text);
	},
	enviarChat:function(){
		Ti.API.info("El mensaje a enviar es "+principal.mensaje);
		var currentTime = new Date();
		var hours = currentTime.getHours();
		var minutes = currentTime.getMinutes();
		var view = Titanium.UI.createView({
						 	     width:Ti.UI.SIZE,
								 height:Ti.UI.SIZE,
								 backgroundColor:"#F2F2F2",
								 borderWidth:1, 
							  	 borderRadius:4,
							  	 left:60,
							  	 right:6
							});
					var label = Ti.UI.createLabel({
								  left:6,
								  top:8,
								  text: $.respuestachat.getValue()+"  "+hours +":"+minutes,
  								  textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
  								  touchEnabled:false,
								  color: '#000',
								  font: { fontFamily:'Roboto-Regular', fontSize: Ti.UI.SIZE, fontWeight:'normal' },
								});
					view.add(label);
					$.respuestas.add(view);
		sockws.enviarChat($.respuestachat.getValue());
		$.respuestachat.setValue("");
		principal.closeModales();
	},
	closeModales:function(){
		if(!$.NuevoSer.getVisible()){
			$.NuevoSer.visible=true;
		}
		if($.chat.getVisible()){
			$.chat.visible=false;
		}
		
	},
	mensajeChat:function(data){
		//intent.putExtra('message_to_echo', 'chat');
		//service.start();
		var currentTime = new Date();
		var hours = currentTime.getHours();
		var minutes = currentTime.getMinutes();
		var view = Titanium.UI.createView({
						 	   width:Ti.UI.SIZE,
								 height:Ti.UI.SIZE,
								 backgroundColor:"#A9F5A9",
								 borderWidth:1, 
							  	 borderRadius:4,
							  	 left:6,
							  	 right:60,
							  	 
							});
					var label = Ti.UI.createLabel({
								  left:6,
								  top:8,
								  text: data.mensaje+"  "+hours +":"+minutes,
  								  textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
  								  touchEnabled:false,
								  color: '#000',
								  font: { fontFamily:'Roboto-Regular', fontSize: Ti.UI.SIZE, fontWeight:'normal' },
								});
					view.add(label);
					$.respuestas.add(view);
					$.NuevoSer.visible=false;
					$.chat.visible=true;
	},
	limpiarChat:function(){
		$.respuestas.removeAllChildren();
	},
	cambiaTitulo:function(){
		menu.setTitulo("Servicios x");
	},
	enviarLlegada:function(){
		ServHandler.informarLlegada();
	},
	openServicio:function(message){
		 Titanium.API.info('vamos a prender el servicio ');
		 var intent = Ti.Android.createIntent({
		    action : Ti.Android.ACTION_MAIN,
		    className : 'com.udriver.udconductorv1.UdriveAllyActivity',
		    flags : Ti.Android.FLAG_ACTIVITY_NEW_TASK
		});
		 
		intent.addCategory(Ti.Android.CATEGORY_LAUNCHER);
		Ti.Android.currentActivity.startActivity(intent);
		switch(message.accion){
			case "nuevo":
				Alloy.Globals.sonido.play();
				Ti.Media.vibrate([5000,100,5000]);
			break;
			case "chat":
				Alloy.Globals.chat.play();
				Ti.Media.vibrate([5000,100,5000]);
			break;
		
		}
		//intent.putExtra('message_to_echo', 'nuevo');
		//service.start();
		//Alloy.Globals.sonido.play();
	},
	showAlerta:function(datos){
		$.labelalertas.setText("");
		$.labelalertas.setText(datos.texto);
		$.alertasmodal.visible=true;
		//intent.putExtra('message_to_echo', 'chat');
		//service.start();
	},
	cerrarAlerta:function(){
		$.alertasmodal.visible=false;
	},
	enviarPosCliente:function(e){
		 if (!e.success || e.error) {
           //  alert("Por favor encender el GPS y volver a intentarlo.");
            //Ti.API.debug(JSON.stringify(e));
            //Ti.API.debug(e);
            return;
        }
		Ti.API.info("se diparo el evento = " + JSON.stringify(e.coords));
		sockws.sendPostoCliente({
            "coords" : e.coords
        });
	},
	showDistancia:function(e){
		var unit = "K";
	if (!e.success || e.error) {
		//alert("No podemos ubicarte, verifica la señal de datos y que el gps este encendido.");
		//Ti.API.debug(JSON.stringify(e));
		//Ti.API.debug(e);
		return;
	}
	Ti.Geolocation.removeEventListener('location', updatePositionDistancia);
	var servicio= Ti.App.Properties.getList('pasajerodat');
	var radlat1 = Math.PI * e.coords.latitude / 180;
	var radlat2 = Math.PI * servicio.latitud / 180;
	var radlon1 = Math.PI * e.coords.longitude / 180;
	var radlon2 = Math.PI * servicio.longitud / 180;
	var theta = e.coords.longitude - servicio.longitud;
	var radtheta = Math.PI * theta / 180;
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist);
	dist = dist * 180 / Math.PI;
	dist = dist * 60 * 1.1515;
	if (unit == "K") {
		dist = dist * 1.609344;
		
	}
	if (unit == "N") {
		dist = dist * 0.8684;
	}
		var num = dist.toFixed(2);
		$.distaciapas.setText('Navega con waze '+num+' Km');
		
	}
	
};

function selecDestino(e){
	principal.selecDestino(e);
}

function updatePosition(e) {
        if (!e.success || e.error) {
            alert("Por favor encender el GPS y volver a intentarlo.");
            //Ti.API.debug(JSON.stringify(e));
            //Ti.API.debug(e);
            return;
        }

        Ti.App.fireEvent("app:got.location", {
            "coords" : e.coords
        });
    };

function updatePositionDistancia(e) {
        if (!e.success || e.error) {
            //alert("Por favor encender el GPS y volver a intentarlo.");
            //Ti.API.debug(JSON.stringify(e));
            //Ti.API.debug(e);
            return;
        }

        Ti.App.fireEvent("app:got.distancia", {
            "coords" : e.coords
        });
    };	
 	
function startposVehiculo(){
	if (Ti.Geolocation.locationServicesEnabled) {
    Titanium.Geolocation.purpose = 'Get Current Location';
    Titanium.Geolocation.addEventListener('location', principal.enviarPosCliente);
} else {
    alert('Por favor encienda el GPS');
}
};
function stopposVehiculo(){
	Titanium.Geolocation.removeEventListener('location', principal.enviarPosCliente);
};
 




 function logout(){
 	if(AppData.getConductor().estado=="LIBRE"){
 		AppData.salir();
 		AppPaquete.salir();
   		$.Principal.close();
 	}else{
 		alert("Debe terminar con el servicio actual antes de salir");
 	}
 	
 }

 $.cleanup = function cleanup() {
  $.destroy();
  $.off();
  	$.Principal.removeEventListener('close', $.cleanup);
	$.Principal.removeEventListener('open', principal.verificarPlaca);
    Ti.App.removeEventListener("app:logout", logout);
	Ti.App.removeEventListener('open', principal.configWindow);
	Ti.App.removeEventListener("servicio:rechazado",principal.servicioRechazado);	
	Ti.App.removeEventListener("cliente:cancela",principal.clienteCancela);
	Ti.App.removeEventListener("servicio:estado",principal.showConexion);
	Ti.App.removeEventListener("chat:nuevomensaje",principal.mensajeChat);
	Ti.App.removeEventListener("servicio:mostrarTarifa", principal.mostrarTarifa);
	Ti.App.removeEventListener("menu:cambiatitulo", principal.cambiaTitulo);
	Ti.App.removeEventListener("servicio:alerta", principal.showAlerta);
    Ti.App.removeEventListener("sincronizar:paquetes", principal.updatePaquetes);
    Ti.App.removeEventListener("pasajero.cancela", principal.cancelar);
    Ti.App.removeEventListener("servicio:tomado", principal.Tomado);
    Ti.App.removeEventListener("servicio:cancelado", principal.concelarOk);
    Ti.App.removeEventListener("open:servicio", principal.openServicio);
    Ti.App.removeEventListener("enviarpos:cliente", principal.enviarPosCliente);
    Ti.App.removeEventListener("servicio:stopposVehiculo", stopposVehiculo);
    Ti.App.removeEventListener("servicio:startposVehiculo", startposVehiculo);
};
$.Principal.addEventListener('open', principal.configWindow);
$.Principal.addEventListener('close', $.cleanup);
$.Principal.addEventListener('open', principal.verificarPlaca);
Ti.App.addEventListener("app:logout", logout);
Ti.App.addEventListener("servicio:rechazado",principal.servicioRechazado);
Ti.App.addEventListener("cliente:cancela",principal.clienteCancela);
Ti.App.addEventListener("servicio:estado",principal.showConexion);
Ti.App.addEventListener("chat:nuevomensaje",principal.mensajeChat);
Ti.App.addEventListener("ciudad:origen",principal.setOrigen);
Ti.App.addEventListener("servicio:alerta", principal.showAlerta);
Ti.App.addEventListener("servicio:mostrarTarifa", principal.mostrarTarifa);
Ti.App.addEventListener("menu:cambiatitulo", principal.cambiaTitulo);
Ti.App.addEventListener("sincronizar:paquetes", principal.updatePaquetes);
Ti.App.addEventListener("app:got.distancia", principal.showDistancia);
Ti.App.addEventListener("conductor:cancela", principal.cancelar);
Ti.App.addEventListener("servicio:setfuec", principal.setFuec);
Ti.App.addEventListener("servicio:tomado", principal.Tomado);
Ti.App.addEventListener("servicio:cancelado", principal.concelarOk);
Ti.App.addEventListener("open:servicio", principal.openServicio);
Ti.App.addEventListener("enviarpos:cliente", principal.enviarPosCliente);
Ti.App.addEventListener("servicio:stopposVehiculo", stopposVehiculo);
Ti.App.addEventListener("servicio:startposVehiculo", startposVehiculo);
}
if(Ti.Platform.osname == "android") {
    var initialLaunchPerformed = false;
    Ti.Android.currentActivity.onResume = function() {
        if (!initialLaunchPerformed) {
            initialLaunchPerformed = true;
            return;
        }
        // If we reach this point the root activity is being resumed for the second (or greater time).
        // Re-run the application-specific start-up code again.
       // runApplication();
    };
}
/*var intent = Titanium.Android.createServiceIntent( { url: 'myservice.js' } );
// Service should run its code every 2 seconds.
intent.putExtra('interval', 4000);
// A message that the service should 'echo'
intent.putExtra('message_to_echo', 'Titanium rocks!');

var service = Titanium.Android.createService(intent);
service.addEventListener('resume', function(e) {
	
    Titanium.API.info('Service code resumes, iteration ' + e.iteration);
});
service.addEventListener('pause', function(e) {
	//Alloy.Globals.sonido.play();
	//Ti.Media.vibrate([0,100,200,100,100,100,100,100,200,100,500,100,225, 100]);
    Titanium.API.info('Service code pauses, iteration ' + e.iteration);
    if (e.iteration >0 ) {
        //Titanium.API.info('Service code has run 3 times, will now stop it.');
        service.stop();
    }
});
		//service.start();
*/


