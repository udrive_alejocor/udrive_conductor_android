//
// Action handlers
//
function openWin(){
 // The new cross-platform way to check permissions
	// The first argument is required on iOS and ignored on other platforms
	var hasLocationPermissions = Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS);
	//log.args('Ti.Geolocation.hasLocationPermissions', hasLocationPermissions);

	if (hasLocationPermissions) {
		return alert('You already have permission.');
	}
	
	// The new cross-platform way to request permissions
	// The first argument is required on iOS and ignored on other platforms
	Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS, function(e) {
		//log.args('Ti.Geolocation.requestLocationPermissions', e);

		if (e.success) {

			// Instead, probably call the same method you call if hasLocationPermissions() is true
			//alert('You granted permission.');

		} else if (OS_ANDROID) {
			alert('You denied permission for now, forever or the dialog did not show at all because it you denied forever before.');

		} else {

			// We already check AUTHORIZATION_DENIED earlier so we can be sure it was denied now and not before
			Ti.UI.createAlertDialog({
				title: 'You denied permission.',

				// We also end up here if the NSLocationAlwaysUsageDescription is missing from tiapp.xml in which case e.error will say so
				message: e.error
			}).show();
		}
	});
	
	// This is now the cross-platform way to check permissions.
	// The above is still useful as it provides the reason of denial.
	var hasCameraPermissions = Ti.Media.hasCameraPermissions();
	//log.args('Ti.Media.hasCameraPermissions', hasCameraPermissions);

	if (hasCameraPermissions) {

		// We have to actually use Ti.Media.showCamera for the permissions to be generated
		return Ti.Media.showCamera({
			success: function(e) {
				//log.args('Ti.Media.showCamera:success', e);
			}
		});
	}
	
	
	// You will be prompted to grant to permissions. If you deny either one weird things happen
	Ti.Media.requestCameraPermissions(function(e) {
		//log.args('Ti.Media.requestCameraPermissions', e);

		if (e.success) {

			// Instead, probably call the same method you call if hasCameraPermissions() is true
			//alert('You granted permission.');

		} else if (OS_ANDROID) {
			alert('You don\'t have the required uses-permissions in tiapp.xml or you denied permission for now, forever or the dialog did not show at all because you denied forever before.');

		} else {

			// We already check AUTHORIZATION_DENIED earlier so we can be sure it was denied now and not before
			alert('You denied permission.');
		}
	});
}
function actionLogin(e) {
	if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {
   alert(' No estas conectado a internet, por favor conectate e intentalo de nuevo ');
	} else {
    if (! $.inputUsername.value || ! $.inputPassword.value) {
        var dialog = Ti.UI.createAlertDialog({
            message: L('formMissingFields', 'Por favor complete todos los campos'),
            ok: 'OK',
            title: L('actionRequired', 'Revisar')
        }).show();
    }
    else {

        $.activityIndicator.show();
        $.buttonLogin.enabled = false;
        var AppData = require('ConductorHandler');
        AppData.login($.inputUsername.value.trim(), $.inputPassword.value.trim(),
            function(response) {
                $.activityIndicator.hide();
                $.buttonLogin.enabled = true;
                if (response.result === "ok") {
                    var indexController = Alloy.createController('index');
                    if (OS_IOS) {
                        Alloy.Globals.navgroup.close();
                        Alloy.Globals.navgroup = null;   
                    } else if (OS_ANDROID) {
                        $.loginForm.close();
                        $.loginForm = null;
                    }                 
                } else {
                    $.inputPassword.value = '';
                    alert(L('error', 'Error') + ':\n' + response.msg);
                }
            });
    
    }
}
//
   
}


//
// View Language
//
$.loginForm.title        = L('login',      'Ingreso');
$.inputUsername.hintText = L('username',   'Usuario');
$.inputPassword.hintText = L('password',   'Contrasena');
$.buttonLogin.title      = L('login',      'Ingresar');


//
// Navigation
//

// Android 
if (OS_ANDROID) {
    $.loginForm.addEventListener('open', function() {
        if($.loginForm.activity) {
            var activity = $.loginForm.activity;
        	// Action Bar
			                if( Ti.Platform.Android.API_LEVEL >= 11 && activity.actionBar) {      
			                    activity.actionBar.hide();           
			                }
			
        }
    });
    
    // Back Button
    $.loginForm.addEventListener('android:back', function() {
        var activity = Ti.Android.currentActivity;
        activity.finish();
    });    
}
$.loginForm.addEventListener('open', openWin);
